<?php if (!defined('BASEPATH')) {exit('No direct script access allowed');}

class Mmodul extends CI_Model{
	function __construct(){
		parent::__construct();
	}
	
	function getdata($type="", $balikan="", $p1="", $p2=""){
		$where = " WHERE 1=1 ";
		switch($type){
			case "data_kamera":
			
				if($this->input->post('ip')){
					$where .=" AND A.ip_camera like '%".$this->input->post('ip')."%' ";
				}
				
				if($this->input->post('tgl_mulai')){
					//$tgl=str_replace("-","",$this->input->post('tgl'));
					if($this->input->post('tgl_akhir')){
						$where .=" AND A.tgl between '".$this->input->post('tgl_mulai')."' AND '".$this->input->post('tgl_akhir')."' ";
					}else{
						$where .=" AND A.tgl between '".$this->input->post('tgl_mulai')."' AND '".$this->input->post('tgl_mulai')."' ";
					}
					
					
				}
				$sql="SELECT A.*,B.kategori FROM tbl_data_kamera A 
					  LEFT JOIN cl_kategori B ON A.cl_kategori_id=B.id
				".$where." ORDER BY A.tgl DESC ";
			break;
			case "data_kendaraan":
				if($this->input->post('plat_nomor')){
					$where .=" AND A.plat_number like '%".$this->input->post('plat_nomor')."%' ";
				}
				if($this->input->post('speed')){
					if($this->input->post('speed')=='<60'){
						$where .=" AND A.speed < 60 ";
					}
					if($this->input->post('speed')=='61-80'){
						$where .=" AND A.speed between 61 AND 80 ";
					}
					if($this->input->post('speed')=='>80'){
						$where .=" AND A.speed >80 ";
					}
				}
				if($this->input->post('ip')){
					$where .=" AND A.file_foto like '%".$this->input->post('ip')."%' ";
				}
				
				if($this->input->post('tgl_mulai')){
					//$tgl=str_replace("-","",$this->input->post('tgl'));
					if($this->input->post('tgl_akhir')){
						$where .=" AND A.capture_date between '".$this->input->post('tgl_mulai')."' AND '".$this->input->post('tgl_akhir')."' ";
					}else{
						$where .=" AND A.capture_date between '".$this->input->post('tgl_mulai')."' AND '".$this->input->post('tgl_mulai')."' ";
					}
					
					
				}
				$sql="SELECT A.* FROM tbl_data_kendaraan A ".$where." ORDER BY A.id DESC ";
			break;
			case "gettot_datakendaraan":
				$sql="SELECT COUNT(plat_number) as tot_kendaraan from tbl_data_kendaraan ";
				return $this->db->query($sql)->row('tot_kendaraan');
			break;
			case "getdatakendaraan":
				if($this->input->post('plat_nomor')){
					$where .=" AND A.plat_number like '%".$this->input->post('plat_nomor')."%' ";
				}
				if($this->input->post('ip')){
					$where .=" AND A.file_foto like '%".$this->input->post('ip')."%' ";
				}
				
				if($this->input->post('tgl')){
					$tgl=str_replace("-","",$this->input->post('tgl'));
					$where .=" AND A.capture_time like '%".$tgl."%' ";
				}
				/*$sql="SELECT A.* FROM (
						SELECT ROW_NUMBER() OVER (ORDER BY A.id DESC) as rowID, A.* FROM tbl_data_kendaraan A ".$where."
						) AS A WHERE A.rowID BETWEEN 1 AND 10 ";
				*/
				$sql="SELECT  A.* FROM tbl_data_kendaraan A ".$where." limit 0,10";
				//echo $sql;exit;
			break;
			
			case "cl_sub_unit":
				if($this->input->post('key')){
					$where .=" AND (A.sub_unit like '%".$this->input->post('key')."%')";
				}
				if($p1=='edit'){$where .=" AND A.id=".$this->input->post('id');}
				$sql="SELECT A.*,A.sub_unit as txt FROM cl_sub_unit A ".$where;
				if($p1=='get'){
					$kon=" AND A.cl_unit_id=".$this->input->post('v2');
					$sql = $sql.$kon;
				}
			break;
			case "user_group":
				if($this->input->post('key')){
					$where .=" AND (A.user_group like '%".$this->input->post('key')."%')";
				}
				$sql="SELECT A.*,A.user_group as txt FROM cl_user_group A ".$where;
			break;
			case "departemen":
				if($this->input->post('key')){
					$where .=" AND (A.departemen like '%".$this->input->post('key')."%' OR A.deskripsi like '%".$this->input->post('key')."%')";
				}
				$sql="SELECT A.*,A.departemen as txt FROM cl_departemen A ".$where." AND A.status='A' ";
			break;
			case "pengajuan_anggaran":
				if($this->input->post('key')){
					$where .=" AND (E.kode_project like '%".$this->input->post('key')."%' 
							  OR E.nama_project like '%".$this->input->post('key')."%' OR 
							  D.departemen like '%".$this->input->post('key')."%')";
				}
				if($this->auth["cl_jabatan_id"]==1 OR $this->auth["cl_jabatan_id"]==2){
					$where .=" AND D.id='".$this->auth["cl_departemen"]."'";
				}
				if($this->auth["cl_jabatan_id"]==3){
					$where .=" AND A.nama_user='".$this->auth["nama_user"]."'";
				}
				$sql="SELECT A.*,E.nama_project,D.departemen as posisi,B.nama_lengkap
					FROM tbl_h_pengajuan A
					LEFT JOIN tbl_user B ON A.nama_user=B.nama_user
					LEFT JOIN tbl_tahapan_sop C ON A.tbl_tahapan_sop_id=C.id
					LEFT JOIN cl_departemen D ON C.cl_departemen_id=D.id
					LEFT JOIN cl_project E ON A.kode_project=E.kode_project ".$where." AND A.flag='P'";
					
				$rs=$this->db->query($sql)->result_array();
				foreach($rs as $x=>$v){
					$sql="SELECT * FROM tbl_status_pengajuan 
						  WHERE tbl_h_pengajuan_id=".$v["id"]." 
						  AND tbl_tahapan_sop_id=".$v["tbl_tahapan_sop_id"];
					$ex=$this->db->query($sql)->row_array();
					if(isset($ex["status"]))$rs[$x]["status_appr"]=$ex["status"];
					else $rs[$x]["status_appr"]='P';
				}
				return json_encode($rs);
			break;
			case "user":
				$sql = " 
					SELECT A.*
					FROM tbl_user A WHERE A.email='".$p1."' OR nama_user='".$p1."'
				";
			break;
			case "user_na":
				if($this->input->post('key')){
					$where .=" AND (nama_lengkap like '%".$this->input->post('key')."%')";
				}
				if($p1=='edit'){
					$where .=" AND A.id=".$this->input->post('id');
				}
				$sql = " 
					SELECT A.*,B.user_group
					FROM tbl_user A 
					LEFT JOIN cl_user_group B ON A.cl_user_group_id=B.id
					".$where;
				//echo $sql;
			break;
			case "info_user":
				if($this->input->post('key')){
					$where .=" AND (nama_lengkap like '%".$this->input->post('key')."%' OR karpeg like '%".$this->input->post('key')."%')";
				}
				if($p1=='edit'){
					$where .=" AND A.id=".$this->input->post('id');
				}
				$sql = " 
					SELECT A.*,B.user_group,C.jabatan,D.departemen,E.unit,F.sub_unit
					FROM tbl_user A 
					LEFT JOIN cl_user_group B ON A.cl_user_group_id=B.id
					LEFT JOIN cl_jabatan C ON A.cl_jabatan_id=C.id
					LEFT JOIN cl_departemen D ON A.cl_departemen=D.id
					LEFT JOIN cl_unit E ON A.cl_unit_id=E.id
					LEFT JOIN cl_sub_unit F ON A.cl_sub_unit_id=F.id ".$where;
				//echo $sql;
			break;
			case "project":
				if($this->input->post('key')){
					$where .=" AND (kode_project like '%".$this->input->post('key')."%' OR nama_project like '%".$this->input->post('key')."%')";
				}
				$sql="SELECT * FROM cl_project ".$where." AND status='A' ";
			break;
			case "barang":
				if($this->input->post('key')){
					$where .=" AND (nama_barang like '%".$this->input->post('key')."%')";
				}
				$sql="SELECT * FROM cl_item_barang ".$where." AND status='A' ";
			break;

		}
		if($balikan == 'row_array'){
			return $this->result_query($sql,'row_array');
		}elseif($balikan == 'result_array'){
			return $this->result_query($sql);
		}else{
			return $this->result_query($sql,'json');
		}
	}
	function result_query($sql,$type=""){
		switch($type){
			case "json":
				$page = (integer) (($this->input->post('page')) ? $this->input->post('page') : "1");
				$limit = (integer) (($this->input->post('rows')) ? $this->input->post('rows') : "10");
				$count = $this->db->query($sql)->num_rows();
				if( $count >0 ) { $total_pages = ceil($count/$limit); } else { $total_pages = 0; } 
				if ($page > $total_pages) $page=$total_pages; 
				$start = $limit*$page - $limit; // do not put $limit*($page - 1)
				if($start<0) $start=0;
				  
				$sql = $sql . " LIMIT $start,$limit";
			
				$data=$this->db->query($sql)->result_array();  
						
				if($data){
				   $responce = new stdClass();
				   $responce->rows= $data;
				   $responce->total =$count;
				   return json_encode($responce);
				}else{ 
				   $responce = new stdClass();
				   $responce->rows = 0;
				   $responce->total = 0;
				   return json_encode($responce);
				} 
			break;
			case "row_obj":return $this->db->query($sql)->row();break;
			case "row_array":return $this->db->query($sql)->row_array();break;
			default:return $this->db->query($sql)->result_array();break;
		}
	}
	
	// GOYZ CROTZZZ
	function get_mutu($nilai){
		$mutu="";
		if($nilai < 261)$mutu='D';
		else if($nilai >=261 && $nilai < 340)$mutu='C';
		else if($nilai >=341 && $nilai < 420)$mutu='B';
		else $mutu='A';
		return $mutu;
	}
	function simpan_data($table,$data,$get_id=""){ //$sts_crud --> STATUS NYEE INSERT, UPDATE, DELETE
		//echo $table;exit;
		
		$this->db->trans_begin();
		$post = array();
		$id = $this->input->post('id');
		$field_id = "id";
		$sts_crud = $this->input->post('sts_crud');
		unset($data['sts_crud']);
		
		switch ($table){
			case "tbl_seting_camera":
				//print_r($data);exit;
				$this->db->query('delete from tbl_server_camera');
				$ip=$data["ip"];
				$dt=array();
				foreach($ip as $x=>$v){
					$dt[]=array('ip'=>$v,
								'deskripsi'=>$data["deskripsi"][$x],
								'port'=>$data["port"][$x],
								'usr'=>$data["usr"][$x],
								'pwd'=>$data["pwd"][$x],
								'create_date'=>date('Y-m-d H:i:s'),
								'create_by'=>$this->auth["nama_user"]
					);
					$path="__repo/stream/".$v;
					if(!file_exists($path))mkdir($path, 0777, true);
				}
				$this->db->insert_batch('tbl_server_camera',$dt);
				
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "tbl_seting_path":
				$sql="DELETE FROM tbl_seting_path ";
				$this->db->query($sql);
				unset($data["id"]);
				$this->db->insert('tbl_seting_path',$data);
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "tbl_user_cabang":
				$data["password"]=$this->encrypt->encode($data['password']);
				$rs=$this->db->get_where('tbl_user_cabang',array('nama_user'=>$data["nama_user"]))->row_array();
				if($sts_crud=='add'){
					if(isset($rs["id"])){echo 2;exit;}
				}elseif($sts_crud=='edit'){
					$this->db->delete('tbl_user_cabang',array('nama_user'=>$data["nama_user"]));
					$sts_crud='add';
				}
			break;
			//case ""tbl_kriteria_penilaian"
			case "penilaian_staff_tk":
				//print_r($data);exit;
				$dt_h=array('tgl_penilaian'=>date('Y-m-d H:i:s'),
							'nama_penilai'=>$this->auth["nama_user"],
							'cl_periode_id'=>$data["cl_periode_id"],
							'tahun'=>date('Y'),
							'tbl_user_id'=>$data["tbl_user_id"],
							'tot_skor'=>$data["tot_skor"],
							'hasil_evaluasi'=>$data["e_tot_skor"],
							'result'=>$data["skor_desc_adm"],
							'nilai_mutu'=>$data["nilai_mutu_adm"],
							'flag'=>'F',
							'create_date'=>date('Y-m-d H:i:s'),
							'create_by'=>$this->auth["nama_user"]
				);
				$skor=$data["skor"];
				
				if($sts_crud=='add'){
					$this->db->insert('tbl_penilaian_staff_tk',$dt_h);
					$id=$this->db->insert_id();
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_teknisi',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_teknisi',$dt_d);
				}else{
					$this->db->update('tbl_penilaian_staff_tk',$dt_h,array('id'=>$id));
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_teknisi',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_teknisi',$dt_d);
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "penilaian_staff_reg":
				//print_r($data);exit;
				$dt_h=array('tgl_penilaian'=>date('Y-m-d H:i:s'),
							'nama_penilai'=>$this->auth["nama_user"],
							'cl_periode_id'=>$data["cl_periode_id"],
							'tahun'=>date('Y'),
							'tbl_user_id'=>$data["tbl_user_id"],
							'tot_skor'=>$data["tot_skor"],
							'hasil_evaluasi'=>$data["e_tot_skor"],
							'result'=>$data["skor_desc_adm"],
							'nilai_mutu'=>$data["nilai_mutu_adm"],
							'flag'=>'F',
							'create_date'=>date('Y-m-d H:i:s'),
							'create_by'=>$this->auth["nama_user"]
				);
				$skor=$data["skor"];
				
				if($sts_crud=='add'){
					$this->db->insert('tbl_penilaian_staff_reg',$dt_h);
					$id=$this->db->insert_id();
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_kinerja',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_kinerja',$dt_d);
				}else{
					$this->db->update('tbl_penilaian_staff_reg',$dt_h,array('id'=>$id));
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_kinerja',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_kinerja',$dt_d);
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "ganti_pwd":
				$table="tbl_user";
				$data["password"]=$this->encrypt->encode($data['pwd_baru']);
				unset($data['pwd_baru']);
				unset($data['pwd_lama']);
				unset($data['u_pwd_baru']);
			break;
			case "hapus_agenda":
				$table='tbl_agenda';
			break;
			case "penilaian_new":
				//print_r($data);exit;
				$flag_penilai=$data["flag_penilai"];
				if($flag_penilai==1)$bobot=50;
				if($flag_penilai==2)$bobot=30;
				if($flag_penilai==3)$bobot=20;
				$score=((float)$data["tot_skor"] * $bobot)/100;
				
				
				
				
				$dt_h=array('tgl_penilaian_'.$data["flag_penilai"]=>date('Y-m-d H:i:s'),
							'nama_penilai_'.$data["flag_penilai"]=>$this->auth["nama_user"],
							//'cl_periode_id'=>$data["cl_periode_id"],
							'tahun'=>date('Y'),
							'tbl_user_id'=>$data["tbl_user_id"],
							'tot_skor_'.$data["flag_penilai"]=>$data["tot_skor"],
							'hasil_evaluasi_'.$data["flag_penilai"]=>$score,
							'flag'=>'P',
							'create_date'=>date('Y-m-d H:i:s'),
							'create_by'=>$this->auth["nama_user"]
				);
				if($flag_penilai==2){
					$dt_h["jml_alpa"]=$data["jml_alpa"];
					$dt_h["jml_sakit"]=$data["jml_sakit"];
					$dt_h["jml_ijin"]=$data["jml_ijin"];
					$dt_h["jml_mangkir"]=$data["jml_mangkir"];
					$dt_h["jml_st"]=$data["jml_st"];
					$dt_h["jml_sp"]=$data["jml_sp"];
				}
				$ex=$this->db->get_where('tbl_penilaian_newcommer',array('tbl_user_id'=>$data["tbl_user_id"]))->row_array();
				if(isset($ex["id"])){
					$id=$ex["id"];
					$sts_crud='edit';
					$t=0;
					for($i=1;$i<=3;$i++){
						if(isset($ex["tot_skor_".$i])){
							if($i==$flag_penilai){
								$t +=(float)$score;
							}else{
								$t +=(float)$ex["hasil_evaluasi_".$i];
							}
						}else{
							if($i==$flag_penilai){
								$t +=(float)$score;
							}else{
								$t +=0;
							}
						}		
					}
					
					if(isset($data["jml_alpa"])){
						if((int)$data["jml_alpa"]>0){
							$a=1*(int)$data["jml_alpa"];
							$t=($t-$a);
						}
					}else{
						if(isset($ex["jml_alpa"])){
							$a=1*(int)$ex["jml_alpa"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_ijin"])){
						if((int)$data["jml_ijin"]>0){
							$a=5*(int)$data["jml_ijin"];
							$t=($t-$a);
						}
					}else{
						if(isset($ex["jml_ijin"])){
							$a=5*(int)$ex["jml_ijin"];
							$t=($t-$a);
						}
					}
					
					if(isset($data["jml_sakit"])){
						if((int)$data["jml_sakit"]>0){
							$a=3*(int)$data["jml_sakit"];
							$t=($t-$a);
						}
					}else{
						if(isset($ex["jml_sakit"])){
							$a=3*(int)$ex["jml_sakit"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_mangkir"])){
						if((int)$data["jml_mangkir"]>0){
							$a=15*(int)$data["jml_mangkir"];
							$t=($t-$a);
						}
					}else{
						if(isset($ex["jml_mangkir"])){
							$a=15*(int)$ex["jml_mangkir"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_st"])){
						if((int)$data["jml_st"]>0){
							$a=30*(int)$data["jml_st"];
							$t=($t-$a);
						}
					}else{
						if(isset($ex["jml_st"])){
							$a=30*(int)$ex["jml_st"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_sp"])){
						if((int)$data["jml_sp"]>0){
							$a=50*(int)$data["jml_sp"];
							$t=($t-$a);
						}
					}else{
						if(isset($ex["jml_sp"])){
							$a=50*(int)$ex["jml_sp"];
							$t=($t-$a);
						}
					}
					
					$dt_h["total_skor"]=$t;
					$dt_h["nilai_mutu"]=$this->get_mutu($t);
				}else{
					$sts_crud='add';
					$t=$score;
					if(isset($data["jml_alpa"])){
						if((int)$data["jml_alpa"]>0){
							$a=1*(int)$data["jml_alpa"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_ijin"])){
						if((int)$data["jml_ijin"]>0){
							$a=5*(int)$data["jml_ijin"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_sakit"])){
						if((int)$data["jml_sakit"]>0){
							$a=3*(int)$data["jml_sakit"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_mangkir"])){
						if((int)$data["jml_mangkir"]>0){
							$a=15*(int)$data["jml_mangkir"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_st"])){
						if((int)$data["jml_st"]>0){
							$a=30*(int)$data["jml_st"];
							$t=($t-$a);
						}
					}
					if(isset($data["jml_sp"])){
						if((int)$data["jml_sp"]>0){
							$a=50*(int)$data["jml_sp"];
							$t=($t-$a);
						}
					}
					$dt_h["total_skor"]=$t;
					$dt_h["nilai_mutu"]=$this->get_mutu($score);
				}
				$skor=$data["skor"];
				
				if($sts_crud=='add'){
					$this->db->insert('tbl_penilaian_newcommer',$dt_h);
					$id=$this->db->insert_id();
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_new_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'flag_penilai'=>$flag_penilai,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_new',array('tbl_penilaian_new_id'=>$id,'flag_penilai'=>$flag_penilai));
					$this->db->insert_batch('tbl_d_penilaian_new',$dt_d);
				}else{
					$this->db->update('tbl_penilaian_newcommer',$dt_h,array('id'=>$id));
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_new_id'=>$id,
									'flag_penilai'=>$flag_penilai,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_new',array('tbl_penilaian_new_id'=>$id,'flag_penilai'=>$flag_penilai));
					$this->db->insert_batch('tbl_d_penilaian_new',$dt_d);
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "penilaian_staff":
				//print_r($data);exit;
				$dt_h=array('tgl_penilaian'=>date('Y-m-d H:i:s'),
							'nama_penilai'=>$this->auth["nama_user"],
							'cl_periode_id'=>$data["cl_periode_id"],
							'tahun'=>date('Y'),
							'tbl_user_id'=>$data["tbl_user_id"],
							'tot_skor'=>$data["tot_skor"],
							'hasil_evaluasi'=>$data["e_tot_skor"],
							'result'=>$data["skor_desc_adm"],
							'nilai_mutu'=>$data["nilai_mutu_adm"],
							'flag'=>'F',
							'create_date'=>date('Y-m-d H:i:s'),
							'create_by'=>$this->auth["nama_user"]
				);
				$skor=$data["skor"];
				
				if($sts_crud=='add'){
					$this->db->insert('tbl_penilaian_staff',$dt_h);
					$id=$this->db->insert_id();
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_kinerja',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_kinerja',$dt_d);
				}else{
					$this->db->update('tbl_penilaian_staff',$dt_h,array('id'=>$id));
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_kinerja',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_kinerja',$dt_d);
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "penilaian_pm":
				//print_r($data);exit;
				$dt_h=array('tgl_penilaian'=>date('Y-m-d H:i:s'),
							'cl_departemen_id'=>$this->auth["cl_departemen"],
							'nama_penilai'=>$this->auth["nama_user"],
							'cl_periode_id'=>$data["cl_periode_id"],
							'tahun'=>date('Y'),
							'tbl_user_id'=>$data["tbl_user_id"],
							'tot_skor'=>$data["tot_skor"],
							'hasil_evaluasi'=>$data["e_tot_skor"],
							'result'=>$data["skor_desc_adm"],
							'nilai_mutu'=>$data["nilai_mutu_adm"],
							'flag'=>'F',
							'create_date'=>date('Y-m-d H:i:s'),
							'create_by'=>$this->auth["nama_user"]
				);
				$skor=$data["skor"];
				
				if($sts_crud=='add'){
					$this->db->insert('tbl_penilaian_pm',$dt_h);
					$id=$this->db->insert_id();
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_pm',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_pm',$dt_d);
				}else{
					$this->db->update('tbl_penilaian_pm',$dt_h,array('id'=>$id));
					$dt_d=array();
					foreach($skor as $x=>$v){
						$dt_d[]=array('tbl_penilaian_kinerja_id'=>$id,
									'tbl_kriteria_penilaian_id'=>$x,
									'nilai'=>$v,
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
					}
					$this->db->delete('tbl_d_penilaian_pm',array('tbl_penilaian_kinerja_id'=>$id));
					$this->db->insert_batch('tbl_d_penilaian_pm',$dt_d);
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "tbl_pengumuman":
				if($this->input->post('flag'))$data["flag"]=$this->input->post('flag');
				else $data["flag"]='N';
				
					if($sts_crud!='delete'){
						if($data["flag"]!='N'){
							$isi=$this->input->post('pengumuman');
							$notification = array();
							$notification['title'] = "PENGUMUMAN";
							$notification['flag'] = "pengumuman";
							//$notification['body'] = substr($isi,0,15)."....";
							$notification['body'] = "PENGUMUMAN BARU";
							
							$sql="SELECT A.* FROM tbl_token_firebase A 
								  LEFT JOIN tbl_user B ON A.tbl_user_id=B.id 
								  WHERE B.sts_peg <> 'R'";
							$rs=$this->db->query($sql)->result_array();
							foreach($rs as $x=>$v){
								$this->lib->send_firebase($notification,$v["firebase_token"]);
							}
						
						}
					}
			break;
			case "tbl_user":
				//if(isset($_FILES['ttd']) && $_FILES['ttd']['name']!="")$data["ttd"]=$this->upload_single("ttd","ttd");
				if(!isset($data['status']))$data['status']='T';
				if(isset($data['password']))$data['password']=$this->encrypt->encode($data['password']);
				if(isset($data['nama_user'])){
					$ex=$this->db->get_where('tbl_user',array('nama_user'=>$data["nama_user"]))->row_array();
					if(isset($ex["nama_user"])){echo 2;exit;}
				}
				
				
			break;
			case "appr_lembur":
				$sql="SELECT id+1 as next 
					  FROM tbl_tahapan_sop 
					  WHERE cl_departemen_id=".$this->auth["cl_departemen"];
				$next=$this->db->query($sql)->row("next");
				if($data["flag"]=='F'){
					if($next <=3){
						$dt_h=array('tbl_lembur_id'=>$data["id"],
									'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],
									'cl_departemen_id'=>$this->auth["cl_departemen"],
									'nama_user'=>$this->auth["nama_user"],
									'status'=>$data["flag"],
									'note'=>(isset($data["note"]) ? $data["note"] : ''),
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
						//$dt_d=array();
						$this->db->insert('tbl_ceklist_lembur',$dt_h);//INSERT CEKLIST HEADER
						$id_h=$this->db->insert_id();
						
						$sql="UPDATE tbl_status_lembur SET status='F' 
							  WHERE tbl_lembur_id=".$data["id"];
						$this->db->query($sql);//UPDATE STATUS PO MONITORING
						
						$sql="INSERT INTO tbl_status_lembur (tbl_lembur_id,tbl_tahapan_sop_id,status) 
							  VALUES (".$data["id"].",".$next.",'P')";
						$this->db->query($sql);//INSERT BARU STEP APPROVAL
						
						//UPDATE TAHAPAN NEXT APPROVAL
						$sql="UPDATE tbl_lembur SET tbl_tahapan_sop_id=".$next." 
							  WHERE id=".$data["id"];
						$this->db->query($sql);
						//END UPDATE TAHAPAN
						
						$pengaju_id=$this->input->post('tbl_user_id');
						//$dept_appr=$next;
						$dept_appr=$this->db->get_where('tbl_tahapan_sop',array('id'=>$next))->row('cl_departemen_id');
						$info_usr=$this->db->get_where('tbl_user',array('id'=>$pengaju_id))->row_array();
						$notification = array();
						$notification['title'] = "APPROVE LEMBUR";
						$notification['flag'] = "appr_lembur";
						//$notification['body'] = substr($isi,0,15)."....";
						$notification['body'] = "Pengajuan Lembur Oleh : ".$info_usr["nama_lengkap"];
						
						$info_appr=$this->db->get_where('tbl_user',array('cl_departemen'=>$dept_appr,'cl_jabatan_id'=>1))->result_array();
						foreach($info_appr as $a=>$b){						
							$rs=$this->db->get_where('tbl_token_firebase',array('tbl_user_id'=>$b["id"]))->row_array();
							if(isset($rs["firebase_token"])){
								$this->lib->send_firebase($notification,$rs["firebase_token"]);
							}
						}
					}else{
						//print_r($data);exit;
						$sql="UPDATE tbl_lembur SET flag='F'
							  WHERE id=".$data["id"];
						$this->db->query($sql);
						$dt_h=array('tbl_lembur_id'=>$data["id"],
									'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],
									'cl_departemen_id'=>$this->auth["cl_departemen"],
									'nama_user'=>$this->auth["nama_user"],
									'status'=>$data["flag"],
									'note'=>(isset($data["note"]) ? $data["note"] : ''),
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
						//$dt_d=array();
						$this->db->insert('tbl_ceklist_lembur',$dt_h);//INSERT CEKLIST HEADER
						$sql="UPDATE tbl_status_lembur SET status='F' WHERE tbl_lembur_id=".$data["id"]." AND tbl_tahapan_sop_id=3 ";
						$this->db->query($sql);//INSERT BARU STEP APPROVAL
					}
				
				}else{
					//print_r($data);exit;
					$dt_h=array('tbl_lembur_id'=>$data["id"],
									'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],
									'cl_departemen_id'=>$this->auth["cl_departemen"],
									'nama_user'=>$this->auth["nama_user"],
									'status'=>$data["flag"],
									'note'=>(isset($data["note"]) ? $data["note"] : ''),
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
					);
					$dt_d=array();
					$ex=$this->db->get_where('tbl_ceklist_lembur',array('tbl_lembur_id'=>$data["id"],'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],'cl_departemen_id'=>$this->auth["cl_departemen"]))->row_array();
					if(isset($ex["id"])){
						$id_h=$ex["id"];
						$this->db->update('tbl_ceklist_lembur',$dt_h,array('id'=>$id_h));//UPDATE CEKLIST HEADER
					}else{
						$this->db->insert('tbl_ceklist_lembur',$dt_h);//INSERT CEKLIST HEADER
						$id_h=$this->db->insert_id();
					}
					
					$sql="UPDATE tbl_status_lembur SET status='R' 
							  WHERE tbl_lembur_id=".$data["id"]." AND tbl_tahapan_sop_id=".$this->auth["cl_departemen"];
					$this->db->query($sql);//UPDATE STATUS PENGAJUAN MONITORING
					$sql="UPDATE tbl_lembur SET flag='R'
							  WHERE id=".$data["id"];
					$this->db->query($sql);
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "appr_cuti":
				$sql="SELECT id+1 as next 
					  FROM tbl_tahapan_sop_cuti 
					  WHERE cl_departemen_id=".$this->auth["cl_departemen"];
				$next_id=$this->db->query($sql)->row("next");
				$sql="SELECT *
					  FROM tbl_tahapan_sop_cuti 
					  WHERE id=".$next_id;
				$next=$this->db->query($sql)->row("cl_departemen_id");
				if($data["flag"]=='F'){
					if($next_id <=3){
						$dt_h=array('tbl_cuti_id'=>$data["id"],
									'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],
									'cl_departemen_id'=>$this->auth["cl_departemen"],
									'nama_user'=>$this->auth["nama_user"],
									'status'=>$data["flag"],
									'note'=>(isset($data["note"]) ? $data["note"] : ''),
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
						//$dt_d=array();
						$this->db->insert('tbl_ceklist_cuti',$dt_h);//INSERT CEKLIST HEADER
						$id_h=$this->db->insert_id();
						
						$sql="UPDATE tbl_status_cuti SET status='F' 
							  WHERE tbl_cuti_id=".$data["id"];
						$this->db->query($sql);//UPDATE STATUS PO MONITORING
						
						$sql="INSERT INTO tbl_status_cuti (tbl_cuti_id,tbl_tahapan_sop_id,status) 
							  VALUES (".$data["id"].",".$next.",'P')";
						$this->db->query($sql);//INSERT BARU STEP APPROVAL
						
						//UPDATE TAHAPAN NEXT APPROVAL
						$sql="UPDATE tbl_cuti SET tbl_tahapan_sop_id=".$next." 
							  WHERE id=".$data["id"];
						$this->db->query($sql);
						//END UPDATE TAHAPAN
						
						//UPDATE NOTIF
					/*	$dt_n=array('tbl_h_pengajuan_id'=>$data["tbl_h_pengajuan_po_id"],
								'cl_departement_id'=>$next,
								'flag_read'=>'P',
								'flag_mod'=>'PAO',
								'flag_notif'=>'N',//NEW APPROVAL
								'create_date'=>date('Y-m-d H:i:s'),
								'create_by'=>$this->auth["nama_user"]
								
						);
						$this->db->insert('tbl_notif_dereksi',$dt_n);
						*/
						$pengaju_id=$this->input->post('tbl_user_id');
						$dept_appr=$next;
						$info_usr=$this->db->get_where('tbl_user',array('id'=>$pengaju_id))->row_array();
						$notification = array();
						$notification['title'] = "APPROVE CUTI";
						$notification['flag'] = "appr_cuti";
						//$notification['body'] = substr($isi,0,15)."....";
						$notification['body'] = "Pengajuan Cuti Oleh : ".$info_usr["nama_lengkap"];
						
						$info_appr=$this->db->get_where('tbl_user',array('cl_departemen'=>$dept_appr,'cl_jabatan_id'=>1))->result_array();
						foreach($info_appr as $a=>$b){						
							$rs=$this->db->get_where('tbl_token_firebase',array('tbl_user_id'=>$b["id"]))->row_array();
							if(isset($rs["firebase_token"])){
								$this->lib->send_firebase($notification,$rs["firebase_token"]);
							}
						}
					}else{
						//print_r($data);exit;
						$sql="UPDATE tbl_cuti SET flag='F'
							  WHERE id=".$data["id"];
						$this->db->query($sql);
						$dt_h=array('tbl_cuti_id'=>$data["id"],
									'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],
									'cl_departemen_id'=>$this->auth["cl_departemen"],
									'nama_user'=>$this->auth["nama_user"],
									'status'=>$data["flag"],
									'note'=>(isset($data["note"]) ? $data["note"] : ''),
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
						);
						//$dt_d=array();
						$this->db->insert('tbl_ceklist_cuti',$dt_h);//INSERT CEKLIST HEADER
						$sql="UPDATE tbl_status_cuti SET status='F' WHERE tbl_cuti_id=".$data["id"]." AND tbl_tahapan_sop_id=3 ";
						$this->db->query($sql);//INSERT BARU STEP APPROVAL
						
						
						// SALDO CUTI
						$sql="SELECT * FROM tbl_saldo_cuti 
							  WHERE tbl_user_id =".$data["tbl_user_id"]." AND tahun=".date('Y');
						$rs=$this->db->query($sql)->row_array();
						if(isset($rs["tahun"])){
							
							$jml_cuti=(isset($data["jml_cuti"]) ? $data["jml_cuti"] : 1);
							$sisa=$rs["sisa_cuti"];
							$tot=($sisa - $jml_cuti);
							$tot_jml_cuti=(isset($data["jml_cuti"]) ? $data["jml_cuti"] : 1)+$rs["jml_cuti"];
							$sts_na='edit';
						}else{
							$jml_cuti=(isset($data["jml_cuti"]) ? $data["jml_cuti"] : 1);
							$sisa=12;
							$tot=($sisa - $jml_cuti);
							$tot_jml_cuti=$jml_cuti;
							$sts_na='add';
						}
						
						
						$dt=array('tahun'=>date('Y'),
								  'tbl_user_id'=>$data["tbl_user_id"],
								  'total_cuti'=>12,
								  'jml_cuti'=>$tot_jml_cuti,
								  'sisa_cuti'=>$tot,
								  'create_date'=>date('Y-m-d H:i:s'),
								  'create_by'=>$this->auth["nama_user"]
						);
						//print_r($dt);exit;
						if($sts_na=='add')$this->db->insert('tbl_saldo_cuti',$dt);
						else {
							unset($dt["tahun"]);
							unset($dt["tbl_user_id"]);
							$this->db->update('tbl_saldo_cuti',$dt,array('tahun'=>date('Y'),'tbl_user_id'=>$data["tbl_user_id"]));
						}
					}
				
				}else{
					//print_r($data);exit;
					$dt_h=array('tbl_cuti_id'=>$data["id"],
									'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],
									'cl_departemen_id'=>$this->auth["cl_departemen"],
									'nama_user'=>$this->auth["nama_user"],
									'status'=>$data["flag"],
									'note'=>(isset($data["note"]) ? $data["note"] : ''),
									'create_date'=>date('Y-m-d H:i:s'),
									'create_by'=>$this->auth["nama_user"]
					);
					$dt_d=array();
					$ex=$this->db->get_where('tbl_ceklist_cuti',array('tbl_cuti_id'=>$data["id"],'cl_jabatan_id'=>$this->auth["cl_jabatan_id"],'cl_departemen_id'=>$this->auth["cl_departemen"]))->row_array();
					if(isset($ex["id"])){
						$id_h=$ex["id"];
						$this->db->update('tbl_ceklist_cuti',$dt_h,array('id'=>$id_h));//UPDATE CEKLIST HEADER
					}else{
						$this->db->insert('tbl_ceklist_cuti',$dt_h);//INSERT CEKLIST HEADER
						$id_h=$this->db->insert_id();
					}
					
					$sql="UPDATE tbl_status_cuti SET status='R' 
							  WHERE tbl_cuti_id=".$data["id"]." AND tbl_tahapan_sop_id=".$this->auth["cl_departemen"];
					$this->db->query($sql);//UPDATE STATUS PENGAJUAN MONITORING
					$sql="UPDATE tbl_cuti SET flag='R'
							  WHERE id=".$data["id"];
					$this->db->query($sql);
					
					/*$dt_n=array('tbl_h_pengajuan_id'=>$data["tbl_h_pengajuan_po_id"],
							'flag_read'=>'P',
							'flag_mod'=>'PAO',
							'flag_notif'=>'R',//REVISI
							'create_date'=>date('Y-m-d H:i:s'),
							'create_by'=>$this->auth["nama_user"]
							
					);
					$this->db->insert('tbl_notif',$dt_n);
					*/
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "tbl_peserta_hapus":
				//print_r($data);exit;
				$peserta=$data["data"];
				$dt=array();
				foreach($peserta as $x=>$v){
					$this->db->delete('tbl_peserta_training',array('tbl_training_id'=>$this->input->post('id_training'),'tbl_user_id'=>$v["tbl_user_id"]));
				}
				
				//$this->db->insert_batch('tbl_peserta_training',$dt);
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}	
			break;
			case "tbl_peserta":
				//print_r($data);exit;
				$peserta=$data["data"];
				$dt=array();
				foreach($peserta as $x=>$v){
					$dt[]=array('tbl_training_id'=>$this->input->post('id_training'),
								'tbl_user_id'=>$v["id"],
								'join_date'=>date('Y-m-d'),
								'status'=>'A',
								'create_date'=>date('Y-m-d H:i:s'),
								'create_by'=>$this->auth["nama_user"],
					);
				}
				$this->db->delete('tbl_peserta_training',array('tbl_training_id'=>$this->input->post('id_training')));
				$this->db->insert_batch('tbl_peserta_training',$dt);
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}	
			break;
			case "training":
				//print_r($data);exit;
				$table='tbl_training';
				$data["tgl_pembayaran"]=$this->tgl_db($data["tgl_pembayaran"]);
				$data["tgl_pelaksanaan"]=$this->tgl_db($data["tgl_pelaksanaan"]);
				$data["tgl_akhir"]=$this->tgl_db($data["tgl_akhir"]);
				$cl_tugas_peserta_id=array();
				if(!isset($data['flag']))$data['flag']='T';
				if(isset($data['cl_tugas_peserta_id'])){
					$cl_tugas_peserta_id=$data['cl_tugas_peserta_id'];
					unset($data['cl_tugas_peserta_id']);
				}
				if($sts_crud == 'add'){
					$this->db->insert($table,$data);
					$id=$this->db->insert_id();
					if(count($cl_tugas_peserta_id)>0){
						$dt=array();
						foreach($cl_tugas_peserta_id as $x=>$v){
							$dt[]=array('tbl_training_id'=>$id,
										'cl_tugas_peserta_id'=>$v,
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"],
							);
						}
						$this->db->insert_batch('tbl_tugas_training',$dt);
					}
				}elseif($sts_crud == 'edit'){
					$this->db->update($table,$data,array($field_id=>$id));
					if(count($cl_tugas_peserta_id)>0){
						$dt=array();
						foreach($cl_tugas_peserta_id as $x=>$v){
							$dt[]=array('tbl_training_id'=>$id,
										'cl_tugas_peserta_id'=>$v,
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"],
							);
						}
						$this->db->delete('tbl_tugas_training',array('tbl_training_id'=>$id));
						$this->db->insert_batch('tbl_tugas_training',$dt);
					}
				}else{
					$this->db->delete('tbl_tugas_training',array('tbl_training_id'=>$id));
					$this->db->delete('tbl_training',array('id'=>$id));
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}	
				
			break;
			case "pegawai":
				$table='tbl_user';
				//if(isset($data["nama"]))
					//print_r($data);exit;
				$cl_jabatan_id=array();
				$tgl_mulai=array();
				$tgl_akhir=array();
				$ket=array();
				$sts_jj=array();
				$nama=array();
				$nik_kk=array();
				$status_keluarga=array();
				$pendidikan_kk=array();
				$pekerjaan=array();
				$jenis_surat=array();
				$sts_surat=array();
				$id_surat=array();
				//echo "<pre>";print_r($_FILES);exit;
				if(isset($data["id_surat"])){
					$id_surat=$data["id_surat"];
					unset($data["id_surat"]);
				}
				if(isset($data["sts_surat"])){
					$sts_surat=$data["sts_surat"];
					unset($data["sts_surat"]);
				}
				if(isset($data["jenis_surat"])){
					$jenis_surat=$data["jenis_surat"];
					unset($data["jenis_surat"]);
				}
				if(isset($data["cl_jabatan_id_jj"])){
					$cl_jabatan_id=$data["cl_jabatan_id_jj"];
					unset($data["cl_jabatan_id_jj"]);
				}
				if(isset($data["tgl_mulai"])){
					$tgl_mulai=$data["tgl_mulai"];
					unset($data["tgl_mulai"]);
				}
				if(isset($data["tgl_akhir"])){
					$tgl_akhir=$data["tgl_akhir"];
					unset($data["tgl_akhir"]);
				}
				if(isset($data["keterangan"])){
					$ket=$data["keterangan"];
					unset($data["keterangan"]);
				}if(isset($data["status_jj"])){
					$sts_jj=$data["status_jj"];
					unset($data["status_jj"]);
				}
				if(isset($data["nama"])){
					$nama=$data["nama"];
					unset($data["nama"]);
				}
				if(isset($data["jenis_kelamin_kk"])){
					$jenis_kelamin=$data["jenis_kelamin_kk"];
					unset($data["jenis_kelamin_kk"]);
				}
				if(isset($data["nik_kk"])){
					$nik_kk=$data["nik_kk"];
					unset($data["nik_kk"]);
				}
				if(isset($data["status_keluarga"])){
					$status_keluarga=$data["status_keluarga"];
					unset($data["status_keluarga"]);
				}
				if(isset($data["pendidikan_kk"])){
					$pendidikan_kk=$data["pendidikan_kk"];
					unset($data["pendidikan_kk"]);
				}
				if(isset($data["pekerjaan"])){
					$pekerjaan=$data["pekerjaan"];
					//print_r($data);exit;
					unset($data["pekerjaan"]);
				}
				if(isset($_FILES['foto']) && $_FILES['foto']['name']!="")$data["foto"]=$this->upload_single("foto","foto");
				if(isset($_FILES['kk']) && $_FILES['kk']['name']!="")$data["kk"]=$this->upload_single("kk","kk");
				if(isset($_FILES['ktp']) && $_FILES['ktp']['name']!="")$data["ktp"]=$this->upload_single("ktp","ktp");
				if(isset($_FILES['ijasah']) && $_FILES['ijasah']['name']!="")$data["ijasah"]=$this->upload_single("ijasah","ijasah");
				if($sts_crud == 'add'){
					if(isset($data["tgl_lahir"]))$data["tgl_lahir"]=$this->tgl_db($data["tgl_lahir"]);
					$this->db->insert($table, $data);
					$id=$this->db->insert_id();
					$dt=array();
					//print_r($nama);exit;
					foreach($nama as $x=>$v){
						if($v!=""){
							$dt[]=array('tbl_user_id'=>$id,
										'nama'=>$v,
										'nik'=>$nik_kk[$x],
										'jenis_kelamin'=>$jenis_kelamin[$x],
										'status_keluarga'=>$status_keluarga[$x],
										'pendidikan'=>$pendidikan_kk[$x],
										'pekerjaan'=>$pekerjaan[$x],
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"]
							);
						}
					}
					if(count($dt)>0)$this->db->insert_batch('tbl_kk',$dt);
					$dt=array();
					foreach($cl_jabatan_id as $x=>$v){
						if($v!=""){
							$tgl_mul=$this->tgl_db($tgl_mulai[$x]);
							$tgl_ak=$this->tgl_db($tgl_akhir[$x]);
							$dt[]=array('tbl_user_id'=>$id,
										'cl_jabatan_id'=>$v,
										'keterangan'=>(isset($ket[$x]) ? $ket[$x] : null ),
										'status'=>(isset($sts_jj[$x]) ? $sts_jj[$x] : null ),
										'tgl_mulai'=>$tgl_mul,
										'tgl_akhir'=>$tgl_ak,
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"]
							);
						}
					}
					if(count($dt)>0)$this->db->insert_batch('tbl_jenjang_karir',$dt);
					
					$dt=array();
					foreach($jenis_surat as $x=>$v){
						$filename=null;
						if($v!=""){
							if(isset($_FILES['file']) && $_FILES['file']['name'][$x]!=""){
								//$file=$this->upload_single("surat","file");
								$extension = pathinfo($_FILES["file"]['name'][$x], PATHINFO_EXTENSION); 
								$filename =  "surat_".date('YmdHis').'.'.$extension;
								
								$files = $_FILES["file"]['name'][$x];
								$tmp  = $_FILES["file"]['tmp_name'][$x];
								$upload_path='__repo/surat/';
								if(!file_exists($upload_path))mkdir($upload_path, 0777, true);
								if(file_exists($upload_path.$filename)){
									unlink($upload_path.$filename);
									$uploadfile = $upload_path.$filename;
								}else{
									$uploadfile = $upload_path.$filename;
								} 
								//echo $_SERVER["DOCUMENT_ROOT"].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$uploadfile;exit;
								move_uploaded_file($tmp, $uploadfile);
								
							}
							$dt[]=array('jenis_surat'=>$v,
										'tbl_user_id'=>$id,
										'file'=>$filename,
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"]
							);
						}
					}
					if(count($dt)>0)$this->db->insert_batch('tbl_surat_pegawai',$dt);
					
				}elseif($sts_crud == 'edit'){
					if(isset($data["tgl_lahir"]))$data["tgl_lahir"]=$this->tgl_db($data["tgl_lahir"]);
					$this->db->update($table, $data, array($field_id=>$id) );
					$dt=array();
					foreach($nama as $x=>$v){
						if($v!=""){
							$dt[]=array('tbl_user_id'=>$id,
										'nama'=>$v,
										'nik'=>$nik_kk[$x],
										'jenis_kelamin'=>$jenis_kelamin[$x],
										'status_keluarga'=>(isset($status_keluarga[$x]) ? $status_keluarga[$x] : '') ,
										'pendidikan'=>(isset($pendidikan_kk[$x]) ? $pendidikan_kk[$x] : ''),
										'pekerjaan'=>(isset($pekerjaan[$x]) ? $pekerjaan[$x] :''),
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"]
							);
						}
					}
					if(count($dt)>0){
						$this->db->delete('tbl_kk',array('tbl_user_id'=>$id));
						$this->db->insert_batch('tbl_kk',$dt);
					}
					$dt=array();
					foreach($cl_jabatan_id as $x=>$v){
						if($v!=""){
							//if(isset($data["tgl_lahir"]))$data["tgl_lahir"]=$this->tgl_db($data["tgl_lahir"]);
							$tgl_mul=$this->tgl_db($tgl_mulai[$x]);
							$tgl_ak=$this->tgl_db($tgl_akhir[$x]);
							$dt[]=array('tbl_user_id'=>$id,
										'cl_jabatan_id'=>$v,
										'keterangan'=>(isset($ket[$x]) ? $ket[$x] : null ),
										'status'=>(isset($sts_jj[$x]) ? $sts_jj[$x] : null ),
										'tgl_mulai'=>$tgl_mul,
										'tgl_akhir'=>$tgl_ak,
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"]
							);
						}
					}
					if(count($dt)>0){
						$this->db->delete('tbl_jenjang_karir',array('tbl_user_id'=>$id));
						$this->db->insert_batch('tbl_jenjang_karir',$dt);
					}
					
					$dt=array();
					foreach($jenis_surat as $x=>$v){
						
						if($v!=""){
							$dt=array('jenis_surat'=>$v,
										'tbl_user_id'=>$id,
										
										'create_date'=>date('Y-m-d H:i:s'),
										'create_by'=>$this->auth["nama_user"]
							);
							if(isset($_FILES['file']) && $_FILES['file']['name'][$x]!=""){
								//$file=$this->upload_single("surat","file");
								$extension = pathinfo($_FILES["file"]['name'][$x], PATHINFO_EXTENSION); 
								$filename =  "surat_".date('YmdHis').'.'.$extension;
								
								$files = $_FILES["file"]['name'][$x];
								$tmp  = $_FILES["file"]['tmp_name'][$x];
								$upload_path='__repo/surat/';
								if(!file_exists($upload_path))mkdir($upload_path, 0777, true);
								if(file_exists($upload_path.$filename)){
									unlink($upload_path.$filename);
									$uploadfile = $upload_path.$filename;
								}else{
									$uploadfile = $upload_path.$filename;
								} 
								//echo $_SERVER["DOCUMENT_ROOT"].preg_replace('@/+$@','',dirname($_SERVER['SCRIPT_NAME'])).'/'.$uploadfile;exit;
								move_uploaded_file($tmp, $uploadfile);
								$dt["file"]=$filename;
							}
							
							if($sts_surat[$x]=='add'){
								$this->db->insert('tbl_surat_pegawai',$dt);
							}else{
								$this->db->update('tbl_surat_pegawai',$dt,array('id'=>$id_surat[$x]));
							}
						}
					}
					
				}elseif($sts_crud == 'delete'){
					$this->db->delete($table, array($field_id=>$id) );
				}
				
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "penilaian_adm":
				//print_r($data);exit;
				$krit=$data["krit_adm"];
				$skor_adm=$data["skor_adm"];
				$dt_d=array();
				foreach($krit as $x=>$v){
					$dt_d[]=array('tbl_user_id'=>$data["tbl_user_id"],
								  'bulan'=>$data["bulan"],
								  'tbl_kriteria_id'=>$x,
								  'skor'=>$skor_adm[$x],
								  'kat_skor'=>$v,
								  'create_date'=>date('Y-m-d H:i:s'),
								  'create_by'=>$this->auth["nama_user"]
								  );
				}
				$dt_h=array('tbl_user_id'=>$data["tbl_user_id"],
								'bulan'=>$data["bulan"],
								'penilai'=>$this->auth["nama_user"],
								'tgl_nilai'=>date('Y-m-d'),
								'tot_skor'=>$data["tot_skor"],
								'nilai_mutu'=>$data["nilai_mutu_adm"],
								'skor_desc'=>$data["skor_desc_adm"],
								'create_date'=>date('Y-m-d H:i:s'),
								'create_by'=>$this->auth["nama_user"]
				);
				$this->db->delete('tbl_penilaian_adm',array('tbl_user_id'=>$data["tbl_user_id"],'bulan'=>$data["bulan"]));
				$this->db->insert('tbl_penilaian_adm',$dt_h);
				$this->db->delete('tbl_d_penilaian_adm',array('tbl_user_id'=>$data["tbl_user_id"],'bulan'=>$data["bulan"]));
				$this->db->insert_batch('tbl_d_penilaian_adm',$dt_d);
				
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "penilaian_tk":
				//print_r($data);exit;
				$krit=$data["krit_teknis"];
				$skor_tk=$data["skor_teknis"];
				$dt_d=array();
				foreach($krit as $x=>$v){
					$dt_d[]=array('tbl_user_id'=>$data["tbl_user_id"],
								  'bulan'=>$data["bulan"],
								  'tbl_kriteria_id'=>$x,
								  'skor'=>$skor_tk[$x],
								  'kat_skor'=>$v,
								  'create_date'=>date('Y-m-d H:i:s'),
								  'create_by'=>$this->auth["nama_user"]
								  );
				}
				$dt_h=array('tbl_user_id'=>$data["tbl_user_id"],
								'bulan'=>$data["bulan"],
								'penilai'=>$this->auth["nama_user"],
								'tgl_nilai'=>date('Y-m-d'),
								'tot_skor'=>$data["tot_skor"],
								'nilai_mutu'=>$data["nilai_mutu_tk"],
								'skor_desc'=>$data["skor_desc_tk"],
								'create_date'=>date('Y-m-d H:i:s'),
								'create_by'=>$this->auth["nama_user"]
				);
				$this->db->delete('tbl_penilaian_tk',array('tbl_user_id'=>$data["tbl_user_id"],'bulan'=>$data["bulan"]));
				$this->db->insert('tbl_penilaian_tk',$dt_h);
				$this->db->delete('tbl_d_penilaian_tk',array('tbl_user_id'=>$data["tbl_user_id"],'bulan'=>$data["bulan"]));
				$this->db->insert_batch('tbl_d_penilaian_tk',$dt_d);
				
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "penilaian_dr":
				//print_r($data);exit;
				$krit=$data["krit_dr"];
				$skor_dr=$data["skor_dr"];
				$dt_d=array();
				foreach($krit as $x=>$v){
					$dt_d[]=array('tbl_user_id'=>$data["tbl_user_id"],
								  'bulan'=>$data["bulan"],
								  'tbl_kriteria_id'=>$x,
								  'skor'=>$skor_dr[$x],
								  'kat_skor'=>$v,
								  'create_date'=>date('Y-m-d H:i:s'),
								  'create_by'=>$this->auth["nama_user"]
								  );
				}
				$dt_h=array('tbl_user_id'=>$data["tbl_user_id"],
								'bulan'=>$data["bulan"],
								'penilai'=>$this->auth["nama_user"],
								'tgl_nilai'=>date('Y-m-d'),
								'tot_skor'=>$data["tot_skor"],
								'nilai_mutu'=>$data["nilai_mutu_dr"],
								'skor_desc'=>$data["skor_desc_dr"],
								'create_date'=>date('Y-m-d H:i:s'),
								'create_by'=>$this->auth["nama_user"]
				);
				$this->db->delete('tbl_penilaian_dr',array('tbl_user_id'=>$data["tbl_user_id"],'bulan'=>$data["bulan"]));
				$this->db->insert('tbl_penilaian_dr',$dt_h);
				$this->db->delete('tbl_d_penilaian_dr',array('tbl_user_id'=>$data["tbl_user_id"],'bulan'=>$data["bulan"]));
				$this->db->insert_batch('tbl_d_penilaian_dr',$dt_d);
				
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			case "qrcode":
				$table="cl_kantor";
				if($sts_crud=='add'){
					$data["qr_code"]=$this->encrypt->encode(date('YmdHis'));
					$data["file_qr"]=$this->cetak_qr(date('YmdHis').'.png',$data["qr_code"]);
				}else if($sts_crud=='edit'){
					/*$ex=$this->db->get_where('cl_kantor',array('id'=>$id))->row_array();
					if(isset($ex["file_qr"])){
						if(file_exists("__repo/qrcode/".$ex["file_qr"])){
							chmod("__repo/qrcode/".$ex["file_qr"],0777);
							unlink("__repo/qrcode/".$ex["file_qr"]);
						}
					}
					$data["file_qr"]=$this->cetak_qr(date('YmdHis').'.png',$data["qr_code"]);
					*/
				}else{
					$ex=$this->db->get_where('cl_kantor',array('id'=>$id))->row_array();
					if(isset($ex["file_qr"])){
						if(file_exists("__repo/qrcode/".$ex["file_qr"])){
							chmod("__repo/qrcode/".$ex["file_qr"],0777);
							unlink("__repo/qrcode/".$ex["file_qr"]);
						}
					}
				}
			break;
			case "ganti_qr":
				$data["qr_code"]=$this->encrypt->encode(date('YmdHis'));
				$ex=$this->db->get_where('cl_kantor',array('id'=>$id))->row_array();
					if(isset($ex["file_qr"])){
						if(file_exists("__repo/qrcode/".$ex["file_qr"])){
							chmod("__repo/qrcode/".$ex["file_qr"],0777);
							unlink("__repo/qrcode/".$ex["file_qr"]);
						}
					}
					$data["file_qr"]=$this->cetak_qr(date('YmdHis').'.png',$data["qr_code"]);
					$sql="UPDATE cl_kantor set file_qr='".$data["file_qr"]."',qr_code='".$data["qr_code"]."' WHERE id=".$id;
					$this->db->query($sql);
					if($this->db->trans_status() == false){
						$this->db->trans_rollback();
						return 0;
					}else{
						return $this->db->trans_commit();	
					}
			break;
			case "form_ket_absen":
				//echo "<pre>";print_r($data);exit;
				/*$sql="SELECT * FROM tbl_log_absensi 
					  WHERE tgl='".$data["tgl"]."' 
					  AND tbl_user_id=".$data["tbl_user_id"];
				$rs=$this->db->query($sql)->result_array();
				foreach($rs as $x=>$v){
					if($v["flag"]=='M')
				}
				*/
				if($this->input->post('jam_masuk')){
					$sql="SELECT * FROM tbl_log_absensi 
						  WHERE tbl_user_id=".$data["tbl_user_id"]." 
						  AND tgl='".$data["tgl"]."' AND flag='M'";
					$rs=$this->db->query($sql)->row_array();
					if(isset($rs["tbl_user_id"])){
						$sql="UPDATE tbl_log_absensi SET jam='".$data["jam_masuk"]."' 
						WHERE tgl='".$data["tgl"]."' AND tbl_user_id=".$data["tbl_user_id"]." AND flag='M'";
						$this->db->query($sql);
					}else{
						$dt=array('tbl_user_id'=>$data["tbl_user_id"],
								  'tgl'=>$data["tgl"],
								  'jam'=>$data["jam_masuk"],
								  'flag'=>'M',
								  'create_date'=>date('Y-m-d'),
								  'create_by'=>$this->auth["nama_user"]
						);
						$this->db->insert('tbl_log_absensi',$dt);
					}
					
				}//UPDATE JAM MASUK
				if($this->input->post('jam_keluar')){
					$sql="SELECT * FROM tbl_log_absensi 
						  WHERE tbl_user_id=".$data["tbl_user_id"]." 
						  AND tgl='".$data["tgl"]."' AND flag='P'";
					$rs=$this->db->query($sql)->row_array();
					if(isset($rs["tbl_user_id"])){
						$sql="UPDATE tbl_log_absensi SET jam='".$data["jam_keluar"]."' 
						WHERE tgl='".$data["tgl"]."' AND tbl_user_id=".$data["tbl_user_id"]." AND flag='P'";
						$this->db->query($sql);
					}else{
						$dt=array('tbl_user_id'=>$data["tbl_user_id"],
								  'tgl'=>$data["tgl"],
								  'jam'=>$data["jam_keluar"],
								  'flag'=>'P',
								  'create_date'=>date('Y-m-d'),
								  'create_by'=>$this->auth["nama_user"]
						);
						$this->db->insert('tbl_log_absensi',$dt);
					}
				}//UPDATE JAM KELUAR
				if($this->input->post('cl_absen_kode')){
					$sql="SELECT * FROM tbl_keterangan_absen 
						  WHERE tbl_user_id=".$data["tbl_user_id"]." 
						  AND tgl='".$data["tgl"]."'";
					$rs=$this->db->query($sql)->row_array();
					if(isset($rs["id"])){
						$sql="UPDATE tbl_keterangan_absen SET cl_absen_kode='".$data["cl_absen_kode"]."' 
						WHERE tgl='".$data["tgl"]."' AND tbl_user_id=".$data["tbl_user_id"];
						$this->db->query($sql);
					}else{
						$dt=array('tbl_user_id'=>$data["tbl_user_id"],
								  'tgl'=>$data["tgl"],
								  'cl_absen_kode'=>$data["cl_absen_kode"],
								  'create_date'=>date('Y-m-d'),
								  'create_by'=>$this->auth["nama_user"]
						);
						$this->db->insert('tbl_keterangan_absen',$dt);
					}
					
					if($this->input->post('cl_absen_kode')=='C' || $this->input->post('cl_absen_kode')=='A'){
						$dt=array('tbl_user_id'=>$data["tbl_user_id"],
								  'tgl_mulai'=>$data["tgl"],
								  'tgl_akhir'=>$data["tgl"],
								  'jml_cuti'=>1,
								  'ket'=>'Absen',
								  'flag'=>'F',
								  'tbl_tahapan_sop_id'=>2,
								  'create_date'=>date('Y-m-d'),
								  'create_by'=>$this->auth["nama_user"]
						);
						$this->db->insert('tbl_cuti',$dt);
					}
					
				}
				if($this->input->post('keterangan')){
					$sql="SELECT * FROM tbl_keterangan_absen 
						  WHERE tbl_user_id=".$data["tbl_user_id"]." 
						  AND tgl='".$data["tgl"]."'";
					$rs=$this->db->query($sql)->row_array();
					if(isset($rs["id"])){
						$sql="UPDATE tbl_keterangan_absen SET keterangan='".$data["keterangan"]."' 
						WHERE tgl='".$data["tgl"]."' AND tbl_user_id=".$data["tbl_user_id"];
						$this->db->query($sql);
					}else{
						$dt=array('tbl_user_id'=>$data["tbl_user_id"],
								  'tgl'=>$data["tgl"],
								  'keterangan'=>$data["keterangan"],
								  'create_date'=>date('Y-m-d'),
								  'create_by'=>$this->auth["nama_user"]
						);
						$this->db->insert('tbl_keterangan_absen',$dt);
					}
				}
				if($this->db->trans_status() == false){
					$this->db->trans_rollback();
					return 0;
				}else{
					return $this->db->trans_commit();	
				}
			break;
			default:$table = $table;break;
		}
		
		if($sts_crud == 'add'){
			$this->db->insert($table, $data);
		}elseif($sts_crud == 'edit'){
			$this->db->update($table, $data, array($field_id=>$id) );
		}elseif($sts_crud == 'delete'){
			if($table=="tbl_kriteria_penilaian"){
				$this->db->delete("tbl_kriteria_penilaian", array("pid"=>$id) );
			}
			$this->db->delete($table, array($field_id=>$id) );
		}
		
		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			return 0;
		}else{
			return $this->db->trans_commit();	
		}
	}
	
	function upload_single($mod,$object){
		$file=date('YmdHis');
		switch($mod){
			case "file_surat_kuasa": $upload_path='__repo/dok/';$file=$this->auth["id"]."_surat_kuasa_".date('YmdHis'); break;
			case "foto": $upload_path='__repo/foto/';$file="foto_".date('YmdHis'); break;
			case "kk": $upload_path='__repo/kk/';$file="kk_".date('YmdHis'); break;
			case "ktp": $upload_path='__repo/ktp/';$file="ktp_".date('YmdHis'); break;
			case "ijasah": $upload_path='__repo/ijasah/';$file="ijasah_".date('YmdHis'); break;
			case "surat": $upload_path='__repo/surat/';$file="surat_".date('YmdHis'); break;
		}
		
		$upload=$this->lib->uploadnong($upload_path, $object, $file);
		if($upload){return $upload;}
		else{echo 2;exit;}
	}
	function set_flag($mod){
		$this->db->trans_begin();
		$id=$this->input->post('id');
		$flag=$this->input->post('flag');
		switch($mod){
			case "reg":
				$table='tbl_reg';
				$dt=array('flag'=>$flag);
			break;
		}
		if($flag=='D'){
			$this->db->delete($table,array('id'=>$id));
		}else{
			$this->db->update($table,$dt,array('id'=>$id));
		}
		if($this->db->trans_status() == false){
			$this->db->trans_rollback();
			return 0;
		}else{
			return $this->db->trans_commit();	
		}
	}
	function cetak_qr($nama,$t_qr){
		$config['cacheable']    = true; //boolean, the default is true
		$config['cachedir']     = '__repo/qrcode/'; //string, the default is application/cache/
		$config['errorlog']     = '__repo/qrcode/'; //string, the default is application/logs/
		$config['imagedir']     = '__repo/qrcode/'; //direktori penyimpanan qr code
		$config['quality']      = true; //boolean, the default is true
		$config['size']         = '1024'; //interger, the default is 1024
		$config['black']        = array(224,255,255); // array, default is array(255,255,255)
		$config['white']        = array(70,130,180); // array, default is array(0,0,0)
		$this->ciqrcode->initialize($config);
		$image_name=$nama; //buat name dari qr code sesuai dengan nim
		$params['data'] = $t_qr; //data yang akan di jadikan QR CODE
		$params['level'] = 'H'; //H=High
		$params['size'] = 10;
		$params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		if($this->ciqrcode->generate($params))return $nama; // fungsi untuk generate QR CODE
		else return 2;
	}
	function get_total_hari($start, $end) {

		$date = new DateTime($start);
		$endDate = new DateTime($end);

		$leaves = array();
		$t=0;
		while($date <= $endDate ) {
			$year = $date->format('Y');
			$month = $date->format('M');        

			if(!array_key_exists($year, $leaves))
				$leaves[$year] = array();
			if(!array_key_exists($month, $leaves[$year]))
				$leaves[$year][$month] = 0;
			
			$t++;
			$leaves[$year][$month]++;
			$date->modify("+1 day");
		}
		
		
		//return $leaves;
		return $t;
	}
	function tgl_db($tgl){
		$ex=explode('-',$tgl);
		$tgl=$ex[2].'-'.$ex[1].'-'.$ex[0];
		return $tgl;
	}
	function get_total($ip,$res=""){
		$sql="SELECT COUNT(id) as total 
				FROM tbl_data_kendaraan 
				WHERE (file_flat like '%".$ip."%' OR file_foto like '%".$ip."%' ) AND capture_date='".date('Y-m-d')."' ";
		$dt=array();
		$dt['t_kendaraan']=$this->db->query($sql)->row('total');
		$sql1 =$sql." AND speed <=60 ";
		$dt['t_60']=$this->db->query($sql1)->row('total');
		$sql1 =$sql." AND speed BETWEEN 61 AND 80 ";
		$dt['t_80']=$this->db->query($sql1)->row('total');
		$sql1 =$sql." AND speed > 80 ";
		$dt['t_100']=$this->db->query($sql1)->row('total');
		if($res=="")echo json_encode($dt);
		else return $dt;
	}
	function get_total_chart($ip,$tgl){
		$sql="SELECT COUNT(id) as total 
				FROM tbl_data_kendaraan 
				WHERE (file_flat like '%".$ip."%' OR file_foto like '%".$ip."%' ) AND capture_date='".$tgl."' ";
		$dt=array();
		$dt['t_kendaraan']=$this->db->query($sql)->row('total');
		$sql1 =$sql." AND speed <=60 ";
		$dt['t_60']=$this->db->query($sql1)->row('total');
		$sql1 =$sql." AND speed BETWEEN 61 AND 80 ";
		$dt['t_80']=$this->db->query($sql1)->row('total');
		$sql1 =$sql." AND speed > 80 ";
		$dt['t_100']=$this->db->query($sql1)->row('total');
	//	if($res=="")echo json_encode($dt);
		 return $dt;
	}

}