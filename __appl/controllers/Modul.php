<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modul extends CI_Controller {
	function __construct(){
        parent::__construct();
		header("Expires: Mon, 26 Jul 1999 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("If-Modified-Since: Mon, 22 Jan 2008 00:00:00 GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Cache-Control: private");
		header("Pragma: no-cache");
		$this->auth = unserialize(base64_decode($this->session->userdata($this->config->item('user_data'))));
		$this->host	= $this->config->item('base_url');
		//print_r ($this->auth);exit;
		$this->path=$this->db->get('tbl_seting_path')->row_array();
		$this->ip=$this->db->get('tbl_server_camera')->result_array();
		$this->smarty->assign("path", $this->path);
		$this->smarty->assign("ip", $this->ip);
		$this->smarty->assign('host',$this->host);
		$this->smarty->assign('auth', $this->auth);
		$this->load->model('mmodul');
		$this->load->library(array('lib','encrypt'));
		$this->smarty->assign("acak", md5(date('H:i:s')));
		
	}
	public function index()
	{
		//$data=$this->mmodul->getdata('getdatakendaraan','result_array');
		//$this->smarty->assign("data", $data);
		//return $this->smarty->display('home.html');
	//	exit;
		if($this->auth){
			$ip=$this->db->get('tbl_server_camera')->result_array();
			$this->smarty->assign("ip", $ip);
			$this->smarty->assign("jml_row", count($ip));
			$this->smarty->assign("nama_modul", "Setting");
			
			$this->smarty->assign("mod", 'setting');
			$this->smarty->display('home.html');
			//$this->smarty->display('main.html');
		}else{
			if($this->session->flashdata('error')){
				$this->smarty->assign("error", $this->session->flashdata('error'));
			}
			$this->smarty->display('login.html');
		}
		
		//$this->smarty->display('home.html');
	}
	function save(){
		echo "<pre>";print_r($_POST);
	}
	
	function modul($p1,$p2){
		if($this->auth){
			$this->smarty->assign("main", $p1);
			$this->smarty->assign("mod", $p2);
			$temp='template/'.$p2.'.html';
			if(!file_exists($this->config->item('appl').APPPATH.'views/'.$temp)){$this->smarty->display('konstruksi.html');}
			else{$this->smarty->display($temp);}	
		}
	}	
	function get_konten($p1="",$p2=""){
		$sts="add";
		if($this->input->post('editstatus'))$sts=$this->input->post('editstatus');
		$this->smarty->assign("sts_crud", $sts);
		$this->smarty->assign("mod", $p1);
		$temp='main/'.$p1.'.html';
		if(!file_exists($this->config->item('appl').APPPATH.'views/'.$temp)){$temp="konstruksi.html";return $this->smarty->display($temp);}
		switch($p1){
			case "view_evidance":
				$data=array();
				$dt=$this->db->get_where('tbl_data_kendaraan',array('plat_number'=>$this->input->post('plat'),'capture_date'=>$this->input->post('tgl')))->row_array();
				$img_mobil="";
				$img_plat="";
				if (file_exists($dt["file_foto"]))$img_mobil = file_get_contents($dt["file_foto"]);
				if (file_exists($dt["file_flat"]))$img_plat = file_get_contents($dt["file_flat"]);
				$dt=array();
				$data["gbr_mobil"] = base64_encode($img_mobil);
				$data["gbr_plat"] = base64_encode($img_plat);
				$this->smarty->assign("data", $data);
				//echo "<pre>";print_r($data);exit;
				return $this->smarty->display($temp);
			break;
			case "evidance":
				$this->smarty->assign("main", $p1);
				$this->smarty->assign("modul", $temp);
				$this->smarty->assign("nama_modul", "Evidance");
				return $this->smarty->display('home.html');
			break;
			case "grid_user":
				$this->smarty->assign("main", "user");
				$this->smarty->assign("modul", $temp);
				$this->smarty->assign("nama_modul", "User Management");
				return $this->smarty->display('home.html');
			break;
			case "data_kendaraan":
				$this->smarty->assign("main", $p1);
				$this->smarty->assign("modul", $temp);
				$this->smarty->assign("nama_modul", "Real Time Data");
				return $this->smarty->display('home.html');
			break;
			case "setting":
				$ip=$this->db->get('tbl_server_camera')->result_array();
				$this->smarty->assign("ip", $ip);
				$this->smarty->assign("jml_row", count($ip));
				$this->smarty->assign("nama_modul", "Setting");
				$this->smarty->assign("modul", $temp);
				return $this->smarty->display('home.html');
			break;
			case "get_kendaraan":
				$data=$this->mmodul->getdata('getdatakendaraan','result_array');
				$this->smarty->assign("data", $data);
				
			break;
			case "form_ganti_pwd":
				return $this->smarty->display($temp);
			break;
			case "posisi":
				$longi=$this->input->post('longi');
				$lat=$this->input->post('lat');
				$this->smarty->assign("longi", $longi);
				$this->smarty->assign("lat", $lat);
			break;
			case "detil_penilaian_new":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$dt_absen=$this->mmodul->getdata('get_dt_absen','row_array','edit');
				$flag_penilai=1;
				if($this->auth["cl_departemen"]==3 && $this->auth["cl_jabatan_id"]==1 && ($this->auth["cl_user_group_id"]==2 || $this->auth["cl_user_group_id"]==1)){
					$flag_penilai=2;
				}
				if($this->auth["cl_departemen"]==1 && $this->auth["cl_jabatan_id"]==1){
					$flag_penilai=3;
				}
				$aspek=$this->mmodul->getdata('aspek_penilaian_new','result_array',$flag_penilai);
				//print_r($peg);exit;
				
				//echo $flag_penilai.' '.$this->auth["cl_departemen"].' '.$this->auth["cl_jabatan_id"]; 
				$data=$this->mmodul->getdata('get_penilaian_new','result_array',$flag_penilai);
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
				}
				$this->smarty->assign("dt_absen", $dt_absen);
				$this->smarty->assign("flag_penilai", $flag_penilai);
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "detil_penilaian_staff_tk":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				//$bln_desc=$this->get_bulan("satuan",$bulan);
				//$peg=$this->db->get_where('tbl_user',array('id'=>$user))->row_array();
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$aspek=$this->mmodul->getdata('aspek_penilaian_staff_tk','result_array',$peg);
				//echo "<pre>";print_r($aspek);
				$data=$this->mmodul->getdata('get_penilaian_staff_tk','result_array');
				//echo "<pre>";print_r($data);exit;
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "detil_penilaian_staff_reg":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				//$bln_desc=$this->get_bulan("satuan",$bulan);
				//$peg=$this->db->get_where('tbl_user',array('id'=>$user))->row_array();
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$aspek=$this->mmodul->getdata('aspek_penilaian_staff_reg','result_array',$peg);
				
				$data=$this->mmodul->getdata('get_penilaian_staff_reg','result_array');
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "detil_penilaian_staff":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				//$bln_desc=$this->get_bulan("satuan",$bulan);
				//$peg=$this->db->get_where('tbl_user',array('id'=>$user))->row_array();
				$aspek=$this->mmodul->getdata('aspek_penilaian_staff','result_array');
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$data=$this->mmodul->getdata('get_penilaian_staff','result_array');
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				/*$data_tk=$this->mmodul->getdata('penilaian_tk','result_array');
				$data_dr=$this->mmodul->getdata('penilaian_dr','result_array');
				if(isset($data_adm['h']['tbl_user_id']))$this->smarty->assign("sts_crud", 'edit');
				if(isset($data_tk['h']['tbl_user_id']))$this->smarty->assign("sts_crud_tk", 'edit');
				if(isset($data_dr['h']['tbl_user_id']))$this->smarty->assign("sts_crud_dr", 'edit');
				//echo "<pre>";print_r($data_tk);
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("data_adm", $data_adm);
				$this->smarty->assign("data_tk", $data_tk);
				$this->smarty->assign("data_dr", $data_dr);
				*/
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "detil_penilaian_pm":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				//$bln_desc=$this->get_bulan("satuan",$bulan);
				//$peg=$this->db->get_where('tbl_user',array('id'=>$user))->row_array();
				$aspek=$this->mmodul->getdata('aspek_penilaian_pm','result_array');
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$data=$this->mmodul->getdata('get_penilaian_pm','result_array');
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "penilaian_staff_reg":
				$periode=$this->db->get_where('cl_periode',array('flag'=>'ST'))->result_array();
				if(count($periode)<=0){
					echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
					exit;
				}
				$this->smarty->assign("periode", $periode);
			break;
			case "penilaian_staff_tk":
				$periode=$this->db->get_where('cl_periode',array('flag'=>'ST'))->result_array();
				if(count($periode)<=0){
					echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
					exit;
				}
				$this->smarty->assign("periode", $periode);
			break;
			case "penilaian_staff":
				$periode=$this->db->get_where('cl_periode',array('flag'=>'PM'))->result_array();
				if(count($periode)<=0){
					echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
					exit;
				}
				$this->smarty->assign("periode", $periode);
			break;
			case "penilaian_pm":
				$periode=$this->db->get_where('cl_periode',array('flag'=>'PM'))->result_array();
				if(count($periode)<=0){
					echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
					exit;
				}
				$this->smarty->assign("periode", $periode);
			break;
			case "list_result_staff_reg":
				$key=$this->input->post('key');
				$periode=$this->input->post('periode');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_staff_reg','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "list_result_staff_tk":
				$key=$this->input->post('key');
				$periode=$this->input->post('periode');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_staff_tk','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "list_result_staff":
				$key=$this->input->post('key');
				$periode=$this->input->post('periode');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "list_result_pm":
				$key=$this->input->post('key');
				$periode=$this->input->post('periode');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_pm','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_pm','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "list_result_new":
				$key=$this->input->post('key');
				$bulan=$this->input->post('bulan');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_new','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "penilaian_newcommer":
				
			break;
			case "form_kriteria_pm":
				$flag=$this->input->post('flag');
				$pid=null;
				if($this->input->post('pid'))$pid=$this->input->post('pid');
				$unit=$this->db->get('cl_departemen')->result_array();
				$jabatan=$this->db->get('cl_jabatan')->result_array();
				if($sts=='edit'){
					$data=$this->db->get_where('tbl_kriteria_pm',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				$this->smarty->assign("flag", $flag);
				$this->smarty->assign("unit", $unit);
				$this->smarty->assign("pid", $pid);
				$this->smarty->assign("jabatan", $jabatan);
			break;
			case "form_kriteria":
			case "form_kriteria_staff":
			case "form_kriteria_staff_reg":
				$flag=$this->input->post('flag');
				$pid=null;
				if($this->input->post('pid'))$pid=$this->input->post('pid');
				$unit=$this->db->get('cl_departemen')->result_array();
				$jabatan=$this->db->get('cl_jabatan')->result_array();
				if($sts=='edit'){
					$data=$this->db->get_where('tbl_kriteria_penilaian',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				$this->smarty->assign("flag", $flag);
				$this->smarty->assign("unit", $unit);
				$this->smarty->assign("pid", $pid);
				$this->smarty->assign("jabatan", $jabatan);
			break;
			case "get_laporan":
				$this->smarty->assign("lap", $p2);
				switch($p2){
					case "absensi":
						$unit=$this->db->get('cl_unit')->result_array();
						$absen=$this->db->get('cl_absen')->result_array();
						$this->smarty->assign("unit", $unit);
						$this->smarty->assign("absen", $absen);
					break;
					case "cuti":
						$unit=$this->db->get('cl_unit')->result_array();
						$absen=$this->db->get('cl_absen')->result_array();
						$this->smarty->assign("unit", $unit);
						$this->smarty->assign("absen", $absen);
					break;
					case "lembur":
						$unit=$this->db->get('cl_unit')->result_array();
						//$absen=$this->db->get('cl_absen')->result_array();
						$this->smarty->assign("unit", $unit);
						//$this->smarty->assign("absen", $absen);
					break;
					case "penilaian_pm":
						$periode=$this->db->get_where('cl_periode',array('flag'=>'PM'))->result_array();
						if(count($periode)<=0){
							echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
							exit;
						}
						$this->smarty->assign("periode", $periode);
					break;
					case "penilaian_staff":
						$periode=$this->db->get_where('cl_periode',array('flag'=>'ST'))->result_array();
						if(count($periode)<=0){
							echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
							exit;
						}
						$this->smarty->assign("periode", $periode);
					break;
					case "penilaian_teknisi":
						$periode=$this->db->get_where('cl_periode',array('flag'=>'ST'))->result_array();
						if(count($periode)<=0){
							echo "<br><br><br><center> <p style='font-size:15px;font-weight:bold'>HARAP TENTUKAN TERLEBIH DAHULU PERIODE PENILAIAN DI SETIING PERIODE</p></center>";
							exit;
						}
						$this->smarty->assign("periode", $periode);
					break;
				}
			break;
			case "pengumuman":
				if($sts=='edit'){
					$data=$this->mmodul->getdata('pengumuman','row_array','edit');
					$this->smarty->assign("data", $data);
				}
			break;
			case "detil_training":
				$data=$this->mmodul->getdata('tbl_training','row_array','edit');
				$this->smarty->assign("data", $data);
				
			break;
			case "detil_lembur":
				$data=$this->mmodul->getdata('get_lembur','row_array','edit');
				$this->smarty->assign("data", $data);
				//print_r($data);exit
			break;
			case "detil_cuti":
				$data=$this->mmodul->getdata('tbl_cuti','row_array','edit');
				$this->smarty->assign("data", $data);
				//print_r($data);exit
			break;
			case "training":
				$tugas=$this->db->get('cl_tugas_peserta')->result_array();
				$this->smarty->assign("tugas", $tugas);
				if($sts=='edit'){
					$data=$this->mmodul->getdata('tbl_training','row_array','edit');
					$this->smarty->assign("data", $data);
				}
			break;
			case "form_agenda":
				//$data=$this->mmodul->getdata('tbl_agenda','result_array');
				//$this->smarty->assign("data", $data);
				if($sts=='edit'){
					$data=$this->mmodul->getdata('tbl_agenda','row_array','edit');
					$this->smarty->assign("data", $data);
				}
				$tgl=$this->input->post('tgl');
				$this->smarty->assign("tgl", $tgl);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp);
			break;
			case "list_even":
				$data=$this->mmodul->getdata('tbl_agenda','result_array');
				$this->smarty->assign("data", $data);
				return $this->smarty->display($temp);
			break;
			case "kalender":
				//$data=$this->db->get('tbl_agenda')->result_array();
				//$this->smarty->assign("data", $data);
				//return $this->smarty->display($temp);
			break;
			case "detil_pegawai":
				$data=$this->mmodul->getdata('pegawai','row_array','edit');
				$kk=$this->mmodul->getdata('tbl_kk','result_array','edit');
				$jj=$this->mmodul->getdata('tbl_jenjang_karir','result_array','edit');
				$tr=$this->mmodul->getdata('get_training','result_array','edit');
				$sr=$this->mmodul->getdata('tbl_surat_pegawai','result_array','edit');
				$this->smarty->assign("sr", $sr);
				$this->smarty->assign("tr", $tr);
				$this->smarty->assign("data", $data);
				$this->smarty->assign("jj", $jj);
				$this->smarty->assign("kk", $kk);
				return $this->smarty->display($temp);
			break;
			case "pegawai":
				$group=$this->mmodul->getdata('user_group','result_array');
				$jabatan=$this->mmodul->getdata('jabatan','result_array');
				$departemen=$this->mmodul->getdata('departemen','result_array');
				$unit=$this->mmodul->getdata('cl_unit','result_array');
				if($sts=='edit'){
					$data=$this->mmodul->getdata('pegawai','row_array','edit');
					$kk=$this->mmodul->getdata('tbl_kk','result_array','edit');
					$jj=$this->mmodul->getdata('tbl_jenjang_karir','result_array','edit');
					$sr=$this->mmodul->getdata('tbl_surat_pegawai','result_array','edit');
					$this->smarty->assign("jml_row_jj", count($jj));
					$this->smarty->assign("jml_row", count($kk));
					$this->smarty->assign("jml_row_sr", count($sr));
					$this->smarty->assign("jj", $jj);
					$this->smarty->assign("kk", $kk);
					$this->smarty->assign("sr", $sr);
					$this->smarty->assign("data", $data);
				}
				$this->smarty->assign("group", $group);
				$this->smarty->assign("unit", $unit);
				$this->smarty->assign("jabatan", $jabatan);
				$this->smarty->assign("departemen", $departemen);
				return $this->smarty->display($temp);
			break;
			case "detil_penilaian":
				$user=$this->input->post('id');
				$bulan=$this->input->post('par1');
				$bln_desc=$this->get_bulan("satuan",$bulan);
				$peg=$this->db->get_where('tbl_user',array('id'=>$user))->row_array();
				$aspek=$this->mmodul->getdata('aspek_penilaian','result_array');
				$data_adm=$this->mmodul->getdata('penilaian_adm','result_array');
				$data_tk=$this->mmodul->getdata('penilaian_tk','result_array');
				$data_dr=$this->mmodul->getdata('penilaian_dr','result_array');
				if(isset($data_adm['h']['tbl_user_id']))$this->smarty->assign("sts_crud", 'edit');
				if(isset($data_tk['h']['tbl_user_id']))$this->smarty->assign("sts_crud_tk", 'edit');
				if(isset($data_dr['h']['tbl_user_id']))$this->smarty->assign("sts_crud_dr", 'edit');
				//echo "<pre>";print_r($data_tk);
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("data_adm", $data_adm);
				$this->smarty->assign("data_tk", $data_tk);
				$this->smarty->assign("data_dr", $data_dr);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("bln_desc", $bln_desc);
				$this->smarty->assign("bulan", $bulan);
				
				$this->smarty->assign("tbl_user_id", $user);
				return $this->smarty->display($temp); 
			break;
			case "list_result_penilaian":
				$key=$this->input->post('key');
				$bulan=$this->input->post('bulan');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "qrcode":
				if($this->input->post('editstatus')=='edit'){
					$data=$this->mmodul->getdata('cl_kantor','row_array','edit');
					$this->smarty->assign("data", $data);
				}
				return $this->smarty->display($temp); 
			break;
			case "form_ket_absen":
				$absen=$this->mmodul->getdata('cl_absen','result_array');
				$tgl=$this->input->post("par1");
				$tbl_user_id=$this->input->post("id");
				$data=$this->mmodul->getdata('absen_edit','row_array',$tbl_user_id,$tgl);
				if(count($data)>0)$this->smarty->assign("sts_crud", 'edit');
				$ket_absen=$this->mmodul->getdata('ket_edit','row_array',$tbl_user_id,$tgl);
				$this->smarty->assign("absen", $absen);
				$this->smarty->assign("ket_absen", $ket_absen);
				$this->smarty->assign("tgl", $tgl);
				$this->smarty->assign("data", $data);
				$this->smarty->assign("tbl_user_id", $tbl_user_id);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "list_absensi_dash":
				$tgl_lalu=date("Y-m-d");
				$tgl_skr=date("Y-m-d");
				//echo $tgl_lalu."->".$tgl_skr;
				$Date = $this->getDatesFromRange($tgl_lalu, $tgl_skr); 
				$absen=$this->mmodul->getdata('data_absen','result_array',$Date);
				//echo $tgl_lalu."->".$tgl_skr;
				//print_r($Date); 
				$this->smarty->assign("absen", $absen);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				$this->smarty->assign("tgl", $Date);
				return $this->smarty->display($temp); 
			break;
			case "list_absensi":
				$key=$this->input->post('key');
				$tgl_na=$this->input->post('tgl_na');
				
				$unit=$this->input->post('unit');
				
				if($tgl_na!=""){
					$pch=explode("-",$tgl_na);
					$tgl_na=$pch[2]."-".$pch[1]."-".$pch[0];
					$date = new DateTime($tgl_na);
					$date->modify('-0 day');
					$tgl_lalu=$date->format('Y-m-d');;
					$tgl_skr=$tgl_na;
				}else{
					$tgl_lalu=date("Y-m-d", strtotime("-0 days"));
					$tgl_skr=date("Y-m-d");
				}
				//echo $tgl_lalu."->".$tgl_skr;
				$Date = $this->getDatesFromRange($tgl_lalu, $tgl_skr); 
				$absen=$this->mmodul->getdata('data_absen','result_array',$Date);
				//echo $tgl_lalu."->".$tgl_skr;
				//print_r($Date); 
				$this->smarty->assign("absen", $absen);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				$this->smarty->assign("tgl", $Date);
				return $this->smarty->display($temp); 
			break;
			case "penilaian":
				//$unit=$this->db->get('cl_unit')->result_array();
				//$this->smarty->assign("unit", $unit);
				$bulan=$this->get_bulan("all");
				$this->smarty->assign("bulan", $bulan);
				$unit=$this->db->get('cl_unit')->result_array();
				$this->smarty->assign("unit", $unit);
			break;
			case "absensi2":
				$unit=$this->db->get('cl_unit')->result_array();
				$absen=$this->db->get('cl_absen')->result_array();
				$this->smarty->assign("unit", $unit);
				$this->smarty->assign("absen", $absen);
			break;
			case "detil_absensi2":
			case "detil_absensi":
				//$temp='main/detil_absensi.html';
				$this->smarty->assign("id", $this->input->post('id'));
				$this->smarty->assign("tgl", $this->input->post('par1'));
				return $this->smarty->display($temp); 
			 break;
			/*case "user":
				$group=$this->mmodul->getdata('user_group','result_array');
				$jabatan=$this->mmodul->getdata('jabatan','result_array');
				$departemen=$this->mmodul->getdata('departemen','result_array');
				$this->smarty->assign("group", $group);
				$this->smarty->assign("jabatan", $jabatan);
				$this->smarty->assign("departemen", $departemen);
				return $this->smarty->display($temp);
			break;
			*/
			case "user":
				$group=$this->mmodul->getdata('user_group','result_array');
			//	$jabatan=$this->mmodul->getdata('jabatan','result_array');
			//	$departemen=$this->mmodul->getdata('departemen','result_array');
				if($this->input->post('editstatus')=='edit'){
					$data=$this->db->get_where('tbl_user',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				$this->smarty->assign("group", $group);
			//	$this->smarty->assign("jabatan", $jabatan);
			//	$this->smarty->assign("departemen", $departemen);
				return $this->smarty->display($temp);
			break;
			case "user_group":
				if($this->input->post('editstatus')=='edit'){
					$data=$this->db->get_where('cl_user_group',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				return $this->smarty->display($temp);
			break;
			case "jabatan":
				if($this->input->post('editstatus')=='edit'){
					$data=$this->db->get_where('cl_jabatan',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				return $this->smarty->display($temp);
			break;
			case "departemen":
				if($this->input->post('editstatus')=='edit'){
					$data=$this->db->get_where('cl_departemen',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				return $this->smarty->display($temp);
			break;
			case "cl_sub_unit":
				$unit=$this->mmodul->getdata('cl_unit','result_array');
				
				$this->smarty->assign("unit", $unit);
				if($this->input->post('editstatus')=='edit'){
					$data=$this->db->get_where('cl_sub_unit',array('id'=>$this->input->post('id')))->row_array();
					$this->smarty->assign("data", $data);
				}
				return $this->smarty->display($temp);
			break;
			case "pengajuan_anggaran":
				return $this->smarty->display($temp);
			break;
			default:
				if($this->input->post('editstatus')=='edit'){
					$data=$this->mmodul->getdata($p1,'row_array','edit');
					$this->smarty->assign("data", $data);
				}
			break;
		}
		//$temp='main/'.$p1.'.html';
		$this->smarty->assign("temp", $temp);
		//$this->smarty->display('home.html');
		$this->smarty->display($temp);
	}
	function get_grid($p1){
		$temp='template/main.html';
		if($p1=='pegawai'){
			$p=$this->db->get_where('tbl_user',array('sts_peg'=>'A'))->result_array();
			$this->smarty->assign("t_a",count($p));
			$p=$this->db->get_where('tbl_user',array('sts_peg'=>'R'))->result_array();
			$this->smarty->assign("t_r",count($p));
			$p=$this->db->get_where('tbl_user',array('sts_peg'=>'N'))->result_array();
			$this->smarty->assign("t_n",count($p));
			$p=$this->db->get_where('tbl_user',array('sts_peg'=>'K'))->result_array();
			$this->smarty->assign("t_k",count($p));
		}
		$this->smarty->assign("temp", $temp);
		$this->smarty->assign("main", $p1);
		$this->smarty->assign("modul", $p1);
		//$this->smarty->display('home.html');
		$this->smarty->display($temp);
	}
	function getdata($p1){
		echo $this->mmodul->getdata($p1,"json");
	}	
	function cruddata($p1){
		$post = array();
        foreach($_POST as $k=>$v){
			if($this->input->post($k)!=""){
				//$post[$k] = $this->db->escape_str($this->input->post($k));
				$post[$k] = $this->input->post($k);
			}
			
		}
		echo $this->mmodul->simpan_data($p1, $post);
	}
	function tes(){
		$sql="SELECT * FROM tbl_jawaban_responden 
			  WHERE tingkat_kepentingan IS NOT NULL AND tingkat_kinerja IS NOT NULL
			  ORDER BY tbl_responden_id,tbl_pertanyaan_id ASC";
		$res=$this->db->query($sql)->result_array();
		$kat=$this->mmodul->getdata("get_kategori","result_array");
		$resp=$this->mmodul->getdata("responden","result_array");
		$rata=$this->mmodul->getdata("get_rata_rata","result_array");
		$total=$this->mmodul->getdata("get_total","result_array");
		$data=array();
		foreach($res as $v=>$x){
			if(isset($data[$x["tbl_responden_id"]]))$data[$x["tbl_responden_id"]][$x["tbl_pertanyaan_id"]]=array('tk'=>$x["tingkat_kepentingan"],'tn'=>$x["tingkat_kinerja"]);
			else{
				$data[$x["tbl_responden_id"]]=array();
				$data[$x["tbl_responden_id"]][$x["tbl_pertanyaan_id"]]=array('tk'=>$x["tingkat_kepentingan"],'tn'=>$x["tingkat_kinerja"]);
			}
				
			
		}
		$this->smarty->assign("rata", $rata);
		$this->smarty->assign("total", $total);
		$this->smarty->assign("resp", $resp);
		$this->smarty->assign("kat", $kat);
		$this->smarty->assign("data", $data);
		$this->smarty->display('bo/rekap.html');
		echo "<pre>";print_r($data);
	}
	
	
	function export_excel(){
		$mod=$this->input->post('mod');
		//print_r($_POST);exit;
		switch($mod){
			
			case "data_kendaraan":
				$rsl=$this->mmodul->getdata('data_kendaraan','result_array');
				$arrCol[] = array('urutan'=>1, 'nilai'=>'Plat Number.','fontsize'=> '12', 'bold'=>true, 'namanya'=>'plat_number', 'format'=>'string');
				$arrCol[] = array('urutan'=>2, 'nilai'=>'Speed ','fontsize'=> '12', 'bold'=>true,'namanya'=>'speed','format'=>'string');
				$arrCol[] = array('urutan'=>3, 'nilai'=>'Capture Date','fontsize'=> '12', 'bold'=>true, 'namanya'=>'capture_date','format'=>'string');
				$arrCol[] = array('urutan'=>4, 'nilai'=>'Capture Time','fontsize'=> '12', 'bold'=>true,'namanya'=>'capture_time','format'=>'string');
				
				$arrExcel = array('sNAMESS'=>'List', 'sFILNAM'=>'export_'.date('YmdHis'),'col'=>$arrCol, 'rsl'=>$rsl);
			break;
			
			case "penilaian_new":
				$temp='cetak/list_result_new.html';
				$key=$this->input->post('key');
				$cetak=$this->input->post('cetak');
				$bulan=$this->input->post('bulan');
				$unit=$this->input->post('unit');
			//	$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_new','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("cetak", $cetak);
				//$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				header("Content-type: application/vnd-ms-excel");
				header("Content-Disposition: attachment; filename=penilaiannewcommer_".date("His").".xls");
				$html=$this->smarty->fetch($temp);
				echo $html;exit;
				//return $this->smarty->display($temp); 
			break;
		}

		$this->lib->buat_excel($arrExcel);	
	} 
	function get_combo(){
		$mod=$this->input->post('v');
		$val=$this->input->post('v3');
		$bind=$this->input->post('v2');
		$data=$this->mmodul->getdata($mod,'result_array','get',$bind);
		$opt="<option value=''>--Pilih--</option>";
		
		foreach($data as $v){
			if($v['id']==$val)$sel="selected"; else $sel="";
			$opt .="<option value='".$v['id']."' ".$sel.">".$v['txt']."</option>";
		}
		echo $opt;
	}
	function download_temp($p1=""){
		$this->load->helper('download');
		switch($p1){
			case "formulir":
				$data = file_get_contents('/opt/aplikasi/eproc/__repo/template/formulir_keikutsertaan.doc');
				$name = 'formulir_keikutsertaan.doc';
			break;
			case "pakta_integritas":
				$data = file_get_contents('/opt/aplikasi/eproc/__repo/template/pakta_integritas.doc');
				$name = 'pakta_integritas.doc';
			break;
			case "surat_kuasa":
				$data = file_get_contents('/opt/aplikasi/eproc/__repo/template/surat_kuasa.doc');
				$name = 'surat_kuasa.doc';
			break;
			case "minat":
				$data = file_get_contents('/opt/aplikasi/eproc/__repo/template/surat_pernyataan_minat.doc');
				$name = 'surat_pernyataan_minat.doc';
			break;
		}
		if(!file_exists('__repo/template/'.$name)){
			echo "Ooooppppsss File NOT EXIST !!!!!";
		}else{
			force_download($name, $data);
		}
	}
	function appr($p1){
		$temp='main/approve.html';
		$this->smarty->assign("mod", "appr");
		return $this->smarty->display($temp);
	}
	function cetak($p1,$html="",$filename=""){
		$this->load->library('mlpdf');		
		$htmlheader = $this->smarty->fetch("cetak/header.html");
		//echo $htmlheader;exit;
		if($html==""){
			$htmlcontent = $this->smarty->fetch("cetak/konten.html");
		}else{
			$htmlcontent=$html;
		}
		//echo $htmlcontent;exit;
		$pdf = $this->mlpdf->load();
		$spdf = new mPDF('', 'A4', 0, '', 12.7, 12.7, 33, 20, 5, 16, 'L');
		$spdf->ignore_invalid_utf8 = true;
		// bukan sulap bukan sihir sim salabim jadi apa prok prok prok
		$spdf->allow_charset_conversion = true;     // which is already true by default
		$spdf->charset_in = 'iso-8859-1';  // set content encoding to iso
		$spdf->SetDisplayMode('fullpage');		
		$spdf->SetHTMLHeader($htmlheader);
		//$spdf->keep_table_proportions = true;
		$spdf->useSubstitutions=false;
		$spdf->simpleTables=true;
		
		$spdf->SetHTMLFooter('
			<div style="font-family:arial; font-size:8px; text-align:center; font-weight:bold;">
				<table width="100%" style="font-family:arial; font-size:8px;">
					<tr>
						<td width="30%" align="left">
							FM.OPS.01
						</td>
						<td width="40%" align="center">
							Revisi: 00
						</td>
						<td width="30%" align="right">
							Hal. {PAGENO} dari {nbpg}
						</td>
					</tr>
				</table>
			</div>
		');				
		if($filename=="")$filename = date('Ymdhis');
		$spdf->SetProtection(array('print'));				
		$spdf->WriteHTML($htmlcontent); // write the HTML into the PDF
		//$spdf->Output('repositories/Dokumen_LS/LS_PDF/'.$filename.'.pdf', 'F'); // save to file because we can
		$spdf->Output($filename.'.pdf', 'I'); // view file	
	}
	function set_flag($mod){
		echo $this->mmodul->set_flag($mod);
	}
	function getDatesFromRange($start, $end, $format = 'Y-m-d') { 
      
		// Declare an empty array 
		$array = array(); 
		  
		// Variable that store the date interval 
		// of period 1 day 
		$interval = new DateInterval('P1D'); 
	  
		$realEnd = new DateTime($end); 
		$realEnd->add($interval); 
	  
		$period = new DatePeriod(new DateTime($start), $interval, $realEnd); 
	  
		// Use loop to store date into array 
		foreach($period as $date) {                  
			$array[] = $date->format($format);  
		} 
	  
		// Return the array elements 
		return $array; 
	} 
	function get_bulan($type="all",$val=""){
		switch($type){
			case "all":
				$bulan=array(1=>"Januari",
							 2=>"Februari",
							 3=>"Maret",
							 4=>"April",
							 5=>"Mei",
							 6=>"Juni",
							 7=>"Juli",
							 8=>"Agustus",
							 9=>"September",
							 10=>"Oktober",
							 11=>"November",
							 12=>"Desember",
				);
			break;
			case "satuan":
				switch($val){
					case 1:$bulan="Januari";break;
					case 2:$bulan="Februari";break;
					case 3:$bulan="Maret";break;
					case 4:$bulan="April";break;
					case 5:$bulan="Mei";break;
					case 6:$bulan="Juni";break;
					case 7:$bulan="Juli";break;
					case 8:$bulan="Agustus";break;
					case 9:$bulan="September";break;
					case 10:$bulan="Oktober";break;
					case 11:$bulan="November";break;
					case 12:$bulan="Desember";break;
				}
			break;
		}
		return $bulan;
	}
	function laporan($p1){
		$cetak=$this->input->post('cetak');
		$type=$this->input->post('type');
		$unit=$this->input->post('unit');
		$kat=$this->input->post('kat');
		$tgl_mulai=$this->input->post('tgl_mulai');
		$tgl_akhir=$this->input->post('tgl_akhir');
		
		switch($p1){
			case "absensi":
				$temp="cetak/absensi.html";
				if($cetak==0){
					if(isset($tgl_mulai) && $tgl_mulai!="")$tgl_mulai=$tgl_mulai;
					else {
						$tgl_mulai=date('Y-m-d');
						$tgl_akhir=date('Y-m-d');
					}
					$Date = $this->getDatesFromRange($tgl_mulai, $tgl_akhir); 
					$absen=$this->mmodul->getdata('absen_laporan','result_array',$Date);
					$this->smarty->assign("absen", $absen);
					$this->smarty->assign("acak_na", $this->input->post('acak_na'));
					$this->smarty->assign("tgl", $Date);
					//$range=$this->getDatesFromRange($tgl_mulai, $tgl_akhir);
					//echo "<pre>";print_r($range);exit;
				}
			break;
			case "cuti":
				$temp="cetak/cuti.html";
				if($cetak==0){
					$cuti=$this->mmodul->getdata('cuti_laporan','result_array');
					$this->smarty->assign("cuti", $cuti);
				}
			break;
			case "lembur":
				$temp="cetak/lembur.html";
				if($cetak==0){
					$lembur=$this->mmodul->getdata('lembur_laporan','result_array');
					$this->smarty->assign("lembur", $lembur);
				}
			break;
			case "penilaian_new":
				$temp='cetak/list_result_new.html';
				$key=$this->input->post('key');
				$cetak=$this->input->post('cetak');
				$bulan=$this->input->post('bulan');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_new','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("cetak", $cetak);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "penilaian_pm":
				$temp='cetak/list_result_pm.html';
				$key=$this->input->post('key');
				$periode=$this->input->post('periode');
				$cetak=$this->input->post('cetak');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_pm','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_pm','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("cetak", $cetak);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "penilaian_staff":
				$temp='cetak/list_result_staff_reg.html';
				$key=$this->input->post('key');
				$periode=$this->input->post('periode');
				$unit=$this->input->post('unit');
				$cetak=$this->input->post('cetak');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_staff_reg_lap','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("cetak", $cetak);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
			case "penilaian_teknisi":
				$temp='cetak/list_result_tk.html';
				$key=$this->input->post('key');
				$cetak=$this->input->post('cetak');
				$periode=$this->input->post('periode');
				$unit=$this->input->post('unit');
				$kriteria=$this->mmodul->getdata('tbl_kriteria_penilaian','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_staff_tk_lap','result_array');
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				$this->smarty->assign("cetak", $cetak);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				return $this->smarty->display($temp); 
			break;
		}
		return $this->smarty->display($temp);
	}
	function cek_pwd_lama(){
		$pwd_lama=$this->input->post('pwd');
		$ex_pwd=$this->encrypt->decode($this->auth["password"]);
		if($pwd_lama==$ex_pwd){
			echo 1;exit;
		}else{
			echo 2;exit;
		}
	}
	
	function cetak_pdf(){
		$mod=$this->input->post('mod');
		switch($mod){
			case "cetak_detil_penilaian_teknisi":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				//$bln_desc=$this->get_bulan("satuan",$bulan);
				//$peg=$this->db->get_where('tbl_user',array('id'=>$user))->row_array();
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$aspek=$this->mmodul->getdata('aspek_penilaian_staff_tk','result_array',$peg);
				
				$data=$this->mmodul->getdata('get_penilaian_staff_tk','result_array');
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				$html=$this->smarty->fetch("cetak/cetak_detil_penilaian_teknisi.html");
				$filename=$peg["nama_lengkap"]."_".date('His');
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "cetak_detil_penilaian_staff":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				$_POST["periode"]=$periode;
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$aspek=$this->mmodul->getdata('aspek_penilaian_staff_reg','result_array',$peg);
				
				$data=$this->mmodul->getdata('get_penilaian_staff_reg','result_array');
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				$html=$this->smarty->fetch("cetak/cetak_detil_penilaian_staff.html");
				$filename=$peg["nama_lengkap"]."_".date('His');
				//$this->smarty->assign("bln_desc", $bln_desc);
				//$this->smarty->assign("bulan", $bulan);
			break;
			case "cetak_detil_penilaian_new":
				$user=$this->input->post('id');
				$periode=$this->input->post('par1');
				
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$dt_absen=$this->mmodul->getdata('get_dt_absen','row_array','edit');
				$flag_penilai=1;
				if($this->auth["cl_departemen"]==3 && $this->auth["cl_jabatan_id"]==1 && $this->auth["cl_user_group_id"]==2){
					$flag_penilai=2;
				}
				if($this->auth["cl_departemen"]==1 && $this->auth["cl_jabatan_id"]==1){
					$flag_penilai=3;
				}
				//print_r($peg);exit;
				$aspek=$this->mmodul->getdata('aspek_penilaian_new','result_array',$flag_penilai);
				//echo $flag_penilai.' '.$this->auth["cl_departemen"].' '.$this->auth["cl_jabatan_id"]; 
				$penilai=array(1=>"Atasan Langsung",2=>"SDM",3=>"Tecnical Manager");
				$data=array();
				foreach($penilai as $a=>$b){
					$data[$a]=array();
					$data[$a]["data"]=$this->mmodul->getdata('get_penilaian_new','result_array',$a);
					$data[$a]["aspek"]=$this->mmodul->getdata('aspek_penilaian_new','result_array',$a);
				}
				
				//echo "<pre>";print_r($data);exit;
				//$data=$this->mmodul->getdata('get_penilaian_new','result_array',$flag_penilai);
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
				}
				$this->smarty->assign("dt_absen", $dt_absen);
				$this->smarty->assign("flag_penilai", $flag_penilai);
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				//$this->smarty->assign("data", $data);
				$this->smarty->assign("data", $data);
				$this->smarty->assign("penilai", $penilai);
				$html=$this->smarty->fetch("cetak/cetak_detil_penilaian_new.html");
				$filename=$peg["nama_lengkap"]."_".date('His');
			break;
			case "cetak_detil_penilaian_pm":
			//print_r($_POST);
				$flag=array(1=>"IT",2=>"Operasional",5=>"FINANCE");
				$user=$this->input->post('id');
				
				$periode=$this->input->post('par1');
				$aspek=$this->mmodul->getdata('aspek_penilaian_pm_cetak','result_array');
				$peg=$this->mmodul->getdata('info_user','row_array','edit');
				$data=$this->mmodul->getdata('get_penilaian_pm_cetak','result_array');
				$per=$this->db->get_where('cl_periode',array('id'=>$periode))->row_array();
				//echo "<pre>";print_r($aspek);exit;
				if(count($data)>0){
					$this->smarty->assign("sts_crud", 'edit');
					//$this->smarty->assign("id", $data["h"]);
				}
				//REKAP
				//$temp='cetak/list_result_pm.html';
				//$key=$this->input->post('key');
				//$periode=$this->input->post('periode');
				//$cetak=$this->input->post('cetak');
				//$unit=$this->input->post('unit');
				$_POST["periode"]=$periode;
				$kriteria=$this->mmodul->getdata('tbl_kriteria_pm','result_array');
				$data_nilai=$this->mmodul->getdata('data_penilaian_pm_pdf','result_array');
				//echo "<pre>";print_r($data_nilai);exit;
				//$data_nilai=$this->mmodul->getdata('data_penilaian_staff','result_array');
				$this->smarty->assign("data_nilai", $data_nilai);
				$this->smarty->assign("kriteria", $kriteria);
				//$this->smarty->assign("cetak", $cetak);
				//$this->smarty->assign("periode", $periode);
				//$this->smarty->assign("acak_na", $this->input->post('acak_na'));
				
				
				//END REKAP
				
				
				
				
				
				
				
				$this->smarty->assign("per", $per);
				$this->smarty->assign("aspek", $aspek);
				$this->smarty->assign("flag", $flag);
				$this->smarty->assign("peg", $peg);
				$this->smarty->assign("periode", $periode);
				$this->smarty->assign("tbl_user_id", $user);
				$this->smarty->assign("data", $data);
				$html=$this->smarty->fetch("cetak/cetak_detil_penilaian_pm.html");
				$filename=$peg["nama_lengkap"]."_".date('His');
			break;
		}
		echo $this->cetak($mod,$html,$filename);
	}
	
	function get_img(){
		$gbr_mobil=$this->input->post('gb_mobil');
		$gbr_plat=$this->input->post('gb_plat');
		//echo $this->path["path_cut"].$gbr_mobil;exit;
	//echo $gbr_mobil;exit;
		$img_mobil="";
		$img_plat="";
		if (file_exists($gbr_mobil))$img_mobil = file_get_contents($gbr_mobil);
		if (file_exists($gbr_plat))$img_plat = file_get_contents($gbr_plat);
		$dt=array();
		$dt["gbr_mobil"] = base64_encode($img_mobil);
		$dt["gbr_plat"] = base64_encode($img_plat);
		echo json_encode($dt);
	}
	
	function tes_kamera(){
		//$this->smarty->display("tes_kamera.html");
		/*$ch = curl_init('http://admin:hik12345@103.3.77.154:3116/ISAPI/Streaming/channels/2/picture');
		$fp = fopen('__repo/stream/flower.jpg', 'wb');
		curl_setopt($ch, CURLOPT_FILE, $fp);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		fclose($fp);
		*/
		//$url="http://admin:hik12345@103.3.77.154:3116/ISAPI/Streaming/channels/2/picture";
		//$url="http://103.3.77.154:3116/ISAPI/Streaming/channels/2/picture";
		//$img="tes.jpg";
		$username = 'admin';
		$password = 'hik12345';
		 
		$url ="http://103.3.77.154/ISAPI/Streaming/channels/2/picture";
		$port="3116";
		$fh = fopen("__repo/stream/flower.jpg", "w") or die($php_errormsg);
		$ch = curl_init();
		echo "username = " . $username . "<br>";
		echo "password = " . $password . "<br>";
		echo "url = " . $url . "<br><br>";
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_ALL);
		curl_setopt($ch, CURLOPT_PORT, $port);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_FILE, $fh);	 
		$success = curl_exec($ch);
		  if ( !$success ) print "<br><B>Error!!</b><br>";
		$output = curl_exec($ch);
		$info = curl_getinfo($ch);
		curl_exec($ch);
		curl_close($ch);
		//file_get_contents($url, false, $context);
		//file_put_contents($img, file_get_contents($url));
		echo 1;exit;
	}
	function run_kamera($ip="",$port="",$user="",$pwd="",$path=""){
		$ip="10.4.1.181";
		$port="554";
		$user="admin";
		$pwd="hik12345";
		$path="D:/xampp/htdocs/vms/__repo/stream/streaming_.m3u8";
		$sh="D:/xampp/htdocs/vms/__assets/ffmpeg/bin/ffmpeg -v verbose  -i rtsp://$user:$pwd@$ip:$port/Streaming/Channels/2 -vf scale=1920:1080  -vcodec libx264 -r 25 -b:v 2600k -crf 31 -acodec aac  -sc_threshold 0 -f hls  -hls_time 10  -segment_time 5 -hls_list_size 6 $path";
		echo "Starting ffmpeg...\n\n <br>";
		//echo shell_exec("D:/xampp/htdocs/vms/__assets/ffmpeg/bin/stream.bat");
		echo shell_exec($sh);
		echo "<br>Done.\n";
		
		//exec($sh);
	}
	function get_total(){
		$ip=$this->input->post('ip');
		echo $this->mmodul->get_total($ip);
	}
	function tes_chart(){
		$this->smarty->display('tes_chart.html');
	}
	function get_chart($p1=""){
		$ch=array();
		$ip=$this->input->post('ip');
		switch($p1){
			case "klass":
				$ch["name"]="Klasifikasi Kecepatan Kendaraaan";
				$ch["colorByPoint"]=true;
				$ch["data"]=array();
				
				$dt=array();
				$dt[0]["name"]='Kecepatan Kurang dari 60 ';
				$dt[0]["color"]='#00b01d';
				$dt[1]["name"]='Kecepatan 61-80 ';
				$dt[1]["color"]='#f7ff57';
				$dt[2]["name"]='Kecepatan > 80 ';
				$dt[2]["sliced"]=true;
				$dt[2]["selected"]=true;
				$dt[2]["color"]='red';
				
				
				$tot=$this->mmodul->get_total($ip,'get');
				$dt[0]["y"]=(int)$tot["t_60"];
				$dt[1]["y"]=(int)$tot["t_80"];
				$dt[2]["y"]=(int)$tot["t_100"];
				$ch["data"]=$dt;
			break;
			case "t_kendaraan":
				$tgl_mulai=$this->input->post('tgl_mulai');
				$tgl_akhir=$this->input->post('tgl_akhir');
				$datetime = new DateTime($tgl_akhir);
				$datetime->modify('+1 day');
				$tgl_akhir=$datetime->format('Y-m-d');
				
				//echo $tgl_akhir;exit;
				$period = new DatePeriod(
					 new DateTime($tgl_mulai),
					 new DateInterval('P1D'),
					 new DateTime($tgl_akhir)
				);
				$dt_x=array();
				$dt_y=array();
				$dt_60=array();
				$dt_80=array();
				$dt_100=array();
				foreach ($period as $key => $value) {
					//$value->format('Y-m-d')   
					$dt_x[]=$value->format('Y-m-d');
					$tot=$this->mmodul->get_total_chart($ip,$value->format('Y-m-d'));
					$dt_60[]=(int)$tot["t_60"];
					$dt_80[]=(int)$tot["t_80"];
					$dt_100[]=(int)$tot["t_100"];
					
				}
				$dt_y[]=array('name'=>'Speed < 60','data'=>$dt_60);
				$dt_y[]=array('name'=>'Speed 61-80','data'=>$dt_80);
				$dt_y[]=array('name'=>'Speed > 80','data'=>$dt_100);
				$ch["x"]=array('categories'=>$dt_x,'crosshair'=>true);
				$ch["y"]=$dt_y;
				//$dt=array();
				
				echo json_encode($ch);
				exit;
				
				//print_r($ch);exit;
			break;
			
		}
		
		echo json_encode(array($ch));
	}
}
